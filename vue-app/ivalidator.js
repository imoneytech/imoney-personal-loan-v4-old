import Vue from 'vue'
import Vuelidate from 'vuelidate'
Vue.use(Vuelidate)
import {
    required,
    minLength,
    between,
    maxLength,
    minValue,
    email,
    maxValue
} from 'vuelidate/lib/validators'
import {
    regex
} from 'vuelidate/lib/validators/common'
import {
    validationMixin
} from 'vuelidate';

export default class iValidator {

    constructor(oForm, campaign) {
        this.oForm = oForm;
        this.campaign = campaign;
        return this.buildValidationObj();
    }

    buildValidationObj() {
        let that = this;
        let validatorObj = {};
        let erroMessagesObj = {};
        _.map(this.oForm, (index, fieldMachineName) => {
            let field = _.pick(that.campaign.fields, fieldMachineName)[fieldMachineName];
            if ( field && field.options.constraints) {
                _.map(field.options.constraints, (aConstrain) => {
                    let validation = {};
                    let erroMessages = {};
                    switch (aConstrain.name) {
                        case 'NotBlank':
                            erroMessages = {
                                required: aConstrain.params.message
                            }
                            validation = {
                                required
                            };
                            break;
                        case 'Email':
                            erroMessages = {
                                email: aConstrain.params.message
                            }
                            validation = {
                                email
                            };
                            break;
                        case 'Length':
                            erroMessages = {
                                minLength: aConstrain.params.minMessage,
                                maxLength: aConstrain.params.maxMessage
                            };
                            validation = {
                                minLength: minLength(aConstrain.params.min),
                                maxLength: maxLength(aConstrain.params.max)
                            };
                            break;
                        case 'Range':
                            erroMessages = {
                                maxValue: aConstrain.params.maxMessage,
                                minValue: aConstrain.params.minMessage
                            };
                            validation = {
                                maxValue: maxValue(aConstrain.params.max),
                                minValue: minValue(aConstrain.params.min),
                            }
                            break;
                        case 'GreaterThanOrEqual':
                            erroMessages = {
                                minValue: aConstrain.params.message
                            };
                            validation = {
                                minValue: minValue(aConstrain.params.value),
                            }
                            break;
                        case 'Regex':
                            let jappRegex = aConstrain.params.pattern
                            let iRegex = new RegExp(jappRegex.substring(1, jappRegex.length - 1));
                            erroMessages = {
                                regex: aConstrain.params.message
                            }
                            validation = {
                                regex: regex('regex', iRegex)
                            };
                            break;
                    }
                    validatorObj[fieldMachineName] = $.extend({}, validatorObj[fieldMachineName],validation);
                    erroMessagesObj[fieldMachineName] = $.extend({}, erroMessagesObj[fieldMachineName],erroMessages);
                });
            } else {
                validatorObj[fieldMachineName] = {};
                erroMessagesObj[fieldMachineName] = {};
            }
        }, {});
        return {
            validatorObj,
            erroMessagesObj
        }

    }
}