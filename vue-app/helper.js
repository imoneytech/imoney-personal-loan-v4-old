export function submitToJapp(values) {
    return new Promise(function(resolve, reject) {
        new CampaignFactory(region, undefined, function(factory) {
            let cam;
            // FIXME: use nid only  not nid or machine name
            if (Loan.jappMachineName) {
                cam = factory.getByMachineName(Loan.jappMachineName);
            } else {
                console.log("Im here NID")
                cam = factory.getByNid(Loan.nid);
            }

            for (let key in values) {
                cam.setFieldValue(key, values[key]);
            }
            IMDevPropBag.add('prequalify', true);
            cam.submit(function(result, status, params, callback_params) {
                if (status == 'complete') {
                    resolve(result);
                }
            });
        });
    });
}

export function creatArrayRange(start, end) {
    let array = [];

    for (var i = start; i <= end; i++) {
        array.push(i);
    }
    return array;
}

export function sortObjAscending(o) {
    var sorted = {},
        key, a = [];

    for (key in o) {
        if (o.hasOwnProperty(key)) {
            a.push(key);
        }
    }

    a.sort();

    for (key = 0; key < a.length; key++) {
        sorted[a[key]] = o[a[key]];
    }
    return sorted;
}

export function getProductData() {
    let productData = {
        title: ""
    }
    if (Loan !== "undefined") {
        productData.title = Loan.title
    }

    return productData
}