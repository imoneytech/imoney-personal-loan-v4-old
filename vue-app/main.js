import iValidator from './ivalidator';
import Vue from 'vue';
import component from './Container/App.vue'


// Step 1
let oformBasic = {
    name: '',
    email_address: '',
    phone_number: '',
    monthly_salary: '',
    loan_amount: '',
    loan_tenure: '',
    checkbox: null
}

// Step 2
let oformBasicPre = {
    identification_type: '',
    purpose: '',
    identification_no: '',
    state: '',
}

// Step 3
let oformEmployment = {
    employment_status: '',
    employment_type: '',
    joining_date: ''
}

// Step 4
let oformFinance = {
    existing_credit_facilities: '',
    branch_state: '',
    branch: ''
}

let oformRHB = {
    stepOne: {
        identification_no: '',
        marital_status: '',
        dependent: '',
        education_level: '',
        employment_type: '',
        gender: '',
        salutation: '',
        date_of_birth: '',
        race: '',
        branch_state: '',
        Branch: '',
    },
    stepTwo: {
        company_name: '',
        joining_date: '',
        office_address: '',
        office_address_1: '',
        office_address_2: '',
        office_postcode: '',
        office_city: '',
        office_state: '',
        office_phone_number: '',
        occupation: '',
    },
    stepThree: {
        address: '',
        address_1: '',
        address_2: '',
        home_postcode: '',
        city: '',
        home_state: '',
        residential_status: '',
        residential_start_date: ''
    },
    stepFour: {
        loan_purpose: '',
        mother_maiden_name: '',
        emergency_contact_name: '',
        emergency_contact_phone_number: '',
        emergency_relationship: '',
        statement_location: ''
    }
}

let vuelidateObjBasic;

let vuelidateObjBasicFormPre;
let vuelidateObjEmploymentFormPre;
let vuelidateObjFinanceFormPre;

let validateStepOne;
let validateStepTwo;
let validateStepThree;
let validateStepFour;

new CampaignFactory(region, undefined, function(factory) {

    let cam = factory.getByNid(Loan.nid);
    // FIXME: use nid only  not nid or machine name
    // if (Loan.nid == "54") {
    //     cam = factory.getByMachineName('MY_PL_RHB_iMoney_PL_RHB_AIP')
    // } 

    let rawCampaign = factory.getRawOne(cam.id);
    // get Step 1 validation
    vuelidateObjBasic = new iValidator(oformBasic, rawCampaign);

    if (Loan.nid == "54" || Loan.nid == "75") {
        validateStepOne = new iValidator(oformRHB.stepOne, rawCampaign);
        validateStepTwo = new iValidator(oformRHB.stepTwo, rawCampaign);
        validateStepThree = new iValidator(oformRHB.stepThree, rawCampaign);
        validateStepFour = new iValidator(oformRHB.stepFour, rawCampaign);

        Vue.mixin({
            data: function() {
                return {
                    get globalValidationObj() {
                        return {
                            vuelidateObjBasic,
                            validateStepOne,
                            validateStepTwo,
                            validateStepThree,
                            validateStepFour
                        };
                    },
                    triggerEvent(eventName, data) {
                        $.event.trigger(eventName, data);
                    }
                }
            }
        })
    } else {
        // get Step 2 validation
        vuelidateObjBasicFormPre = new iValidator(oformBasicPre, rawCampaign);
        // get Step 3 validation
        vuelidateObjEmploymentFormPre = new iValidator(oformEmployment, rawCampaign);
        // get Step 4 validation
        vuelidateObjFinanceFormPre = new iValidator(oformFinance, rawCampaign);

        Vue.mixin({
            data: function() {
                return {
                    get globalValidationObj() {
                        return {
                            vuelidateObjBasic,
                            vuelidateObjBasicFormPre,
                            vuelidateObjFinanceFormPre,
                            vuelidateObjEmploymentFormPre
                        };
                    },
                    triggerEvent(eventName, data) {
                        $.event.trigger(eventName, data);
                    }
                }
            }
        })
    }
    new Vue(component).$mount('#appTwo');
});