FROM node:8-alpine
LABEL maintainer Ross Affandy <ross.affandy@imoney-group.com>

ARG region

WORKDIR /app
COPY . /app
RUN cp /app/config/app.json.sample /app/config/app.json
RUN NR_NATIVE_METRICS_NO_BUILD=true npm install
RUN if [ "$region" = "ph" ] ; then NR_NATIVE_METRICS_NO_BUILD=true npm run build:prod ; fi
# FIXME: The following ask for public key for bitbucket
# RUN install-capi-client.sh

EXPOSE 3000
CMD [ "npm", "start" ]
