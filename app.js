var newrelic = require('newrelic');
var express = require('express');
var path = require('path');
var logger = require('morgan');

var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
const debug = require('debug')(`personal-loan:App`);

//app.locals.newrelic = newrelic;
var app = express();
//Prettify HTML
//app.locals.pretty = true;

// Translation tools.
//require('./custom_modules/libraries/languages');

// Format tools.
//require('./custom_modules/libraries/format');

/*********************************************************
 ** Check configuration
 **********************************************************/
//Loan and Check Konfig
try {
    global.config = require('konfig')();
    console.log(global.config);

    if (!global.config) {
        throw new Error('Error loading configuration ./config/app.json! Please check if the file exists with correct configuration!');
    }
} catch (e) {
    console.error(e.stack);
    process.exit(1);
}

var routes = require('./routes/index');

//Lib iServer connectivity
var isitemap = require('./custom_modules/iclient/isitemap.js');
var iclient = require('./custom_modules/iclient/iclient.js')(global.config.app.imaster);
var isystem = require('./custom_modules/iclient/isystem.js')(global.config.app);

//Sync system health info
isystem.on('update', function(sysinfo) {
    iclient.setConfig('system', sysinfo);
});

//Sync sitemap
isitemap.on('change', function(sitemap) {
    iclient.setConfig('sitemap', sitemap);
});

//Serves sitemap.json
app.use(isitemap.sitemap());
//Enf of iServer connectivity

// view engine setup
//app.engine('html', swig.renderFile);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');


app.set('view cache', true);
//swig.setDefaults({ cache: false });
//app.set('view cache', true);

//TODO: on production env always enable swig cache
//swig.setDefaults({ cache: false });

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));

// app.use(logger('combined'));
// // log only 4xx and 5xx responses to console
app.use(logger('dev'))
// app.use(logger('combined', {stream: accessLogStream}))

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

var iCacheManager = require('./custom_modules/iMoneyCacheManager');
app.use('/cache', iCacheManager.router);
app.use(iCacheManager.cacheManager);

app.use((req, res, next) => {
  const test = /\?[^]*\//.test(req.url);
  var url = req.url.split("?")
  if (url[0].substr(-1) === "/" && req.url.length > 1 && !test) {
    res.redirect(301, url[1] ? url[0].slice(0, -1) + "?" + url[1] : req.url.slice(0, -1));
  } else next();
});

app.use('/', routes);



 //catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found'+ req.originalUrl);
    err.status = 404;
    next(err);
});

// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    console.log(err.message);
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('errorfile', {
        message: err.message,
        error: err,
        app: global.config.app,
        bodyClass: 'imoney-' + global.config.app.region,
        region: global.config.app.region,
        lang: global.config.app.lang
    });
});

module.exports = app;
