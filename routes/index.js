'use strict';

var express = require('express');
var app = express();
var router = express.Router();
var request = require('request');
var async = require("async");
var htmlroutes = require('../views/' + config.app.region + '/htmlfqa.js');
var Q = require('q');
var isitemap = require('../custom_modules/iclient/isitemap.js');
var busboy = require('connect-busboy');
var fs = require('fs');
router.use(busboy());
var FeaturedData = [];
var _ = require('underscore');
var removeRoute = require('express-remove-route');
var data;

// apply form fields
var city = config.app.apply_from['city'];
var another_cards = config.app.apply_from['have-another-cards'];
var No_NPWP = config.app.apply_from['No-NPWP'];
data = require('./../custom_modules/PersonalLoanListData.js')(config.app.base_url);
var metadatalisting = require('./../custom_modules/MetaDataListData')(config.app.base_url_meatadata);
var productsCrossSell = require('./../custom_modules/productsCrossSell.js')(config.app.cross_products);

if (config.app.lang == 'en') {
    var language = require('../custom_modules/libraries/Disablelanguages.js');
    router.get('/' + config.app.pl_listing + '/translate/generate/', function(req, res) {
        var jsonData = [];
        res.json(jsonData);
    });
}

router.get('/', function (req, res) {
    res.send('GET request to the homepage');
});

if (config.app.lang == 'id') {
    router.get('/kredit-tanpa-agunan/search', function(req, res, next) {
        res.render('smartsearch_search');
    });

    router.all('/kredit-tanpa-agunan/results', function(req, res, next) {
        res.render('smartsearch_results', {data : smartsearchDate ? JSON.parse(smartsearchDate): []});
    });
}

if (config.app.lang != 'en') {
    // Translation module
    var language = require('../custom_modules/libraries/languages.js');

    router.get('/' + config.app['pl_listing'] + '/translate/', function(req, res) {
        language.potGenerate();
        res.render('../views/translate');
    }).post('/' + config.app['pl_listing'] + '/translate/', function(req, res) {
        var fstream;
        req.pipe(req.busboy);
        req.busboy.on('file', function(fieldname, file, filename) {
            console.log("Uploading: " + filename);
            fstream = fs.createWriteStream('dev/translation/' + config.app.lang + '_' + config.app.region.toUpperCase() + '.po');
            file.pipe(fstream);
            fstream.on('close', function() {
                res.redirect('back');
            });
        });
    });



    router.get('/' + config.app.pl_listing + '/translate/generate/', function(req, res) {
        var jsonData = language.jsonGenerate();
        res.json(jsonData);
    });

    router.get('/' + config.app.pl_listing + '/translate/pot/', function(req, res) {
        console.log('dev/translation/' + config.app.lang + '_' + config.app.region.toUpperCase() + '.pot');
        res.download('dev/translation/' + config.app.lang + '_' + config.app.region.toUpperCase() + '.pot');
    });

    router.get('/' + config.app.pl_listing + '/translate/po/', function(req, res) {
        console.log(config.app.region);
        //console.log('dev/translation/' + config.lang + '_' + config.region.toUpperCase() + '.po');
        res.download('dev/translation/' + config.app.lang + '_' + config.app.region.toUpperCase() + '.po');
    });


    // End of tranlation logic

}

var metadataListPromise = Q.nbind(metadatalisting.fetch, metadatalisting);
metadataListPromise().then(function() {

    if (!metadatalisting.models.length) {
        console.error('Failed to fetch metadata list data!');
        process.exit(1);
    }

    console.log(metadatalisting.models.length + ' metadata loaded for the listing page!');
}, function(err) {
    console.log('metadata list could not be loaded: ' + err.toString());
    process.exit();
});



var crossSellListPromise = Q.nbind(productsCrossSell.fetch, productsCrossSell);
crossSellListPromise().then(function() {
    if (!productsCrossSell.models.length) {
        console.error('Failed to fetch cross sell products list data!');
        process.exit(1);
    }
    console.log(productsCrossSell.models.length + ' cross sell products loaded for the listing page!');
}, function(err) {
    console.log('cross sell products list could not be loaded: ' + err.toString());
    process.exit();
});

var app_id = "428626330554993";
let updated = 0;

function clearCache(path) {
    if (Date.now() - updated > 30000) {
        updated = Date.now();
        data.fetch(function() {
            console.info('Listing data cache cleared! Req sent from:' + path);
            GetFeaturedData(data);
        });

        metadatalisting.fetch(function() {
            console.info('meta data cache cleared! Req sent from:' + path);
        });
    }
}

var iCacheManager = require('../custom_modules/iMoneyCacheManager');
iCacheManager.eventEmitter.on('cacheClear', function(path, req) {
    clearCache(path);
});

function GetFeaturedData(data) {
    console.log('getting FeaturedData ............');
    FeaturedData = [];
    data.forEach(function(loan) {
        if (loan.attributes.field_feature_this_loan == "1") {
            FeaturedData.push(loan.attributes);
        }
    });
}


function returnpageTitle(metadata) {
    var pageTitle;
    try {
        pageTitle = metadata['title'].value;
    } catch (e) {
        pageTitle = "";
    }
    return pageTitle;
}

function returnmetaDescription(metadata) {
    var metaDescription;
    try {
        metaDescription = metadata['description'].value;
    } catch (e) {
        metaDescription = "";
    }
    return metaDescription;
}

function returnmetaKeyword(metadata) {
    var metaKeyword;
    try {
        metaKeyword = metadata['keywords'].value;
    } catch (e) {
        metaKeyword = "";
    }
    return metaKeyword;
}

function returnogTitle(metadata) {
    var ogTitle;
    try {
        ogTitle = metadata['og:title'].value || pageTitle;
    } catch (e) {
        ogTitle = "";
    }
    return ogTitle;
}


function returnog_description(metadata) {
    var og_description;
    try {
        og_description = metadata['og:description'].value;
    } catch (e) {
        og_description = "";
    }
    return og_description;
}


function returncanonicalURL(metadata, pageurl, req) {
    var canonicalURL;
    try {
        canonicalURL = metadata['canonical'].value;
        if (canonicalURL == "") {
            canonicalURL = req.protocol + '://' + req.hostname + "/" + pageurl;
        }
    } catch (e) {
        canonicalURL = req.protocol + '://' + req.hostname + "/" + pageurl;
    }
    return canonicalURL;
}

function returnogImage(metadata) {
    var ogImage;
    try {
        ogImage = metadata['og:image'].value;
    } catch (e) {}
    return ogImage;
}


var productPromise = [];
productPromise = Q.nbind(data.fetch, data);
productPromise().then(function() {
    if (!data.models.length) {
        console.error('Failed to fetch PersonalLoan Listing!');
        process.exit(1);
    }
    console.log(data.models.length + ' Listing pages!');
}, function(err) {
    console.log('Products could not be loaded: ' + err.toString());
    process.exit();
});



var tmp = [];
data.on('add', function(product) {
    //console.log(product.attributes);
    if (product.attributes.field_feature_this_loan == "1") {
        tmp.push(product.attributes)
    }
    FeaturedData = tmp;
    registerRouteFor(product);
});


data.on('remove', function(product) {
    console.log("one product getting removed");
    removeRouteFor(product);
});



function removeRouteFor(loan) {
    console.log("removing : " + loan.attributes.product_path);
    removeRoute(router, "/" + loan.attributes.product_path);
}

function islamic_profit_interest(loan) {
    return loan.islamic_conventional === "Islamic" ? "Profit rate" : "Interest rate"
}

function registerRouteFor(product) {
    var loan = product.attributes;
    console.log(loan.product_path + ' isReady ..!!');
    router.get(isitemap.add('/' + loan.product_path).lastAdded(), function(req, res, next) {
        clearCache('microcache');
        var p = data.get(product.get('nid'));
        var showPreQualify = config.app.prequalifyNids.indexOf(loan.nid);
        var allow6Month = config.app.allow6MonthNids.indexOf(loan.nid);
        loan = p.attributes;
        res.render('../views/personal-loan__product', {
            loan: loan,
            islamic_profit_interest: islamic_profit_interest(loan),
            app_id: app_id,
            siteName: config.app.site_name,
            metaKeyword: returnmetaKeyword(loan.metatags),
            pageTitle: returnpageTitle(loan.metatags),
            metaDescription: returnmetaDescription(loan.metatags),
            ogTitle: returnogTitle(loan.metatags),
            ogDescription: returnog_description(loan.metatags),
            ogImage: returnogImage(loan.metatags),
            canonicalURL: returncanonicalURL(loan.metatags, loan.product_path, req),
            ////////////////////////////////
            showPreQualify: showPreQualify >= 0 ? true : false,
            allow6Month: allow6Month >= 0 ? true : false,
            FeaturedData: FeaturedData.filter(card => card.nid !== loan.nid).slice(0,4),
            Logo: loan.bankLogo,
            Title: loan.title,
            minimumAge: loan.minimumAge,
            maximumAge: loan.maximumAge,
            approval_duration: loan.field_approval_duration,
            maximumAmount: loan.maximumAmount,
            minimumAmount: loan.minimumAmount,
            employmentEligibility: loan.employmentEligibility,
            bodyClass: 'imoney-' + config.app.region + ' page-product-single product-v1',
            ///////////////////////////////////////////////////////
            Currency: config.app.Currency,
            loan_amount: config.app.loan_amount,
            tenure: config.app.tenure,
            lang: config.app.lang,
            Monthly_Salary: config.app.Monthly_Salary,
            Min_value_Salary: config.app.Min_value_Salary,
            Max_value_Salary: config.app.Max_value_Salary,
            interval_Tenure: config.app.interval_Tenure,
            limit_max_Tenure: config.app.max_Tenure,
            Step_salary: config.app.Step_salary,
            region: config.app.region,
            gTag: config.app.gtm,
            language: language,
            tranlateurl: '/' + config.app['pl_listing'] + '/translate/generate',
            thank_you: config.app['pl_thank_you'],
            City: city,
            another_cards: another_cards,
            NPWP: No_NPWP,
            number_spreater: config.app.exchange_number_separator,
            applyformurl: config.app.applyformurl
        });
    });
}

function get_min_max_tenure(data) {
    var personalLoanRates = [];
    data.forEach(function(loan) {
        personalLoanRates.push(loan.attributes.minimumTenure);
        personalLoanRates.push(loan.attributes.maximumTenure);
    });
    var x = {
        "max": Math.max.apply(null, personalLoanRates),
        "min": Math.min.apply(null, personalLoanRates)
    }
    return x;
}


function get_min_max_salary(data) {
    var minSalary = [];
    data.forEach(function(loan) {
        if (loan.attributes.minimumIncome != undefined) {
            minSalary.push(parseFloat(loan.attributes.minimumIncome));
        }
    });
    return Math.min.apply(null, minSalary)
}



config.app.Listing_pages.forEach(function(urls, index) {
    var metadata = [];
    for (var key in config.app.Listing_pages[index]) {
        router.get("/" + key + '/data', function(req, res, next) {
            res.json(data);
        });
        router.get(isitemap.add("/" + key).lastAdded(), function(req, res, next) {
            clearCache(' microcache');
            var pagepath = req.path;
            if (pagepath.substr(-1) == '/') {
                pagepath = pagepath.substr(0, pagepath.length - 1);
                pagepath = pagepath.replace('/', '')
            } else {
                pagepath = req.path;
                pagepath = pagepath.replace('/', '');
            }
            try {
                metadata = metadatalisting.models[0].attributes[pagepath]['reactions']['metatag_context_reaction']['metatags']['und'];
            } catch (e) {
                metadata = [];
            }
            res.render('../views/personal-loan', {
                app_id: app_id,
                lang: config.app.lang,
                siteName: config.app.site_name,
                metaKeyword: returnmetaKeyword(metadata),
                pageTitle: returnpageTitle(metadata),
                metaDescription: returnmetaDescription(metadata),
                ogTitle: returnogTitle(metadata),
                ogDescription: returnog_description(metadata),
                ogImage: returnogImage(metadata),
                canonicalURL: returncanonicalURL(metadata, pagepath, req),
                /////////////////////////////
                app: config.app,
                prequalifyNids: config.app.prequalifyNids,
                islamicpage: config.app.Listing_pages[index][pagepath].islamicpage,
                businesspage: config.app.Listing_pages[index][pagepath].businesspage,
                islamic: config.app.Listing_pages[index][pagepath].islamic, // false means hidden islamic filter
                government: config.app.Listing_pages[index][pagepath].government,
                bodyClass: 'imoney-' + config.app.region,
                gTag: config.app.gtm,
                header_title: config.app.Listing_pages[index][pagepath].header_title,
                header_description: config.app.Listing_pages[index][pagepath].header_description,
                /////////////
                Currency: config.app.Currency,
                loan_amount: config.app.loan_amount,
                tenure: config.app.tenure,
                Monthly_Salary: config.app.Monthly_Salary,
                Min_value_Salary: get_min_max_salary(data),
                Max_value_Salary: config.app.Max_value_Salary,
                Max_value_Tenure: get_min_max_tenure(data).max,
                Min_value_Tenure: get_min_max_tenure(data).min,
                interval_Tenure: config.app.interval_Tenure,
                limit_max_Tenure: config.app.max_Tenure,
                Step_salary: config.app.Step_salary,
                dataUrl: "/" + pagepath + "/data",
                region: config.app.region,
                Fqahtml: htmlroutes[config.app.region][pagepath],
                PersonaLoans: data,
                productsCrossSell: productsCrossSell,
                language: language,
                tranlateurl: '/' + config.app['pl_listing'] + '/translate/generate',
                thank_you: config.app['pl_thank_you'],
                City: city,
                another_cards: another_cards,
                NPWP: No_NPWP,
                number_spreater: config.app.exchange_number_separator,
                Lising_url: pagepath,
                applyformurl: config.app.applyformurl
            });
        });
    }
});

router.get(isitemap.add(config.app['pl_thank_you']).lastAdded(), function(req, res, next) {
    var metadata = [];
    var pagepath = req.path;
    if (pagepath.substr(-1) == '/') {
        pagepath = pagepath.substr(0, pagepath.length - 1);
        pagepath = pagepath.replace('/', '')
    } else {
        pagepath = req.path;
        pagepath = pagepath.replace('/', '');
    }
    try {
        metadata = metadatalisting.models[0].attributes[pagepath]['reactions']['metatag_context_reaction']['metatags']['und'];
    } catch (e) {
        metadata = [];
    }
    res.render('../views/thank-you-' + config.app.region + '/personal-loan-thank-you', {
        bodyClass: 'imoney-' + config.app.region + ' page-cc-product page-thankyou',
        app_id: app_id,
        lang: config.app.lang,
        metaKeyword: returnmetaKeyword(metadata),
        pageTitle: returnpageTitle(metadata),
        metaDescription: returnmetaDescription(metadata),
        ogTitle: returnogTitle(metadata),
        ogDescription: returnog_description(metadata),
        ogImage: returnogImage(metadata),
        canonicalURL: returncanonicalURL(metadata, pagepath, req),
        gTag: config.app.gtm,
        region: config.app.region,
        rssurl: config.app['data-rss-MANAGEMENT'],
        siteName: config.app.site_name,
        language: language,
        prequalify: typeof req.query.prequalify !== 'undefined'
    });
});



if (config.app.region == "my") {
    router.get(isitemap.add('/personal-loan/rhb-easy-pinjaman-ekspres/apply').lastAdded(), function(req, res, next) {
        res.render('../views/easy-rhb-apply', {
            bodyClass: 'imoney-' + config.app.region,
            gTag: config.app.gtm,
            pageTitle: 'RHB Easy Pinjaman Ekspres - Personal Financing | iMoney',
        });
    });

    router.get(isitemap.add('/personal-loan/rhb-easy-pinjaman-ekspres/thank-you').lastAdded(), function(req, res, next) {
        res.render('../views/easy-rhb-thankyou', {
            bodyClass: 'imoney-' + config.app.region,
            gTag: config.app.gtm,
            pageTitle: 'RHB Easy Pinjaman Ekspres - Personal Financing | iMoney'
        });
    });
}



module.exports = router;
