var translate = function (text, context, params) {
  try {
    if (typeof context !== 'undefined' && context != null && context.length) {
      var translated = translateLibrary[context][text].msgstr[0];
    }
    else {
      var translated = translateLibrary[''][text].msgstr[0];
    }
  }
  catch (err) {
    var translated = text;
  }

  // Add params.
  for (var key in params) {
    translated = translated.replace(key, params[key]);
  }

  console.log('translated = '+translated);
  return translated;
};