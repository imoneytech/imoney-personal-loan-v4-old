// @codekit-prepend "../../bower_components/imoney-assets/bower_components/nouislider/distribute/nouislider.min.js";
// @codekit-prepend "../../bower_components/imoney-assets/bower_components/nouislider/documentation/assets/wNumb.js";
// @codekit-prepend "stickykit.min.js";
// @codekit-prepend "../../bower_components/imoney-assets/bower_components/bootstrap-sass/assets/javascripts/bootstrap/transition.js";
// @codekit-prepend "../../bower_components/imoney-assets/bower_components/bootstrap-sass/assets/javascripts/bootstrap/modal.js";
// @codekit-prepend "jquery.easing.min.js";

$(document).ready(function() {
  //---------------------------
  // NOUISLIDER
  //---------------------------

  var slider = document.getElementById("slider-salary");
  var salary = document.getElementById("salary-howmuch");

  noUiSlider.create(slider, {
    start: DefaultMonthly_Salary,
    animate: true,
    step: Step_salary,
    range: {
      min: Math.ceil(Min_value_Salary / 100) * 100,
      max: Max_value_Salary,
    },
    format: wNumb({
      decimals: 0,
    }),
  });

  slider.noUiSlider.on("change", function(value, handle) {
    var imuTrack = new IMUTrack({
      region: "Listing Filter",
      subject: "User",
      action: "Input",
      description: "Salary",
      vertical: "Personal Loan",
      value: value[handle],
    });
    userManager.track(imuTrack);
  });
  angular.element(document).ready(function() {
    slider.noUiSlider.on("update", function(value, handle) {
      var scope = angular.element($("#input-amount")).scope();
      scope.$apply(function() {
        scope.setSalary(value[handle]);
      });

      //scope.update();
      if (value == Max_value_Salary) {
        if (number_spreater) {
          salary.innerHTML =
            "> " +
            Currency +
            value[handle].replace(/\B(?=(\d{3})+(?!\d))/g, ".");
        } else {
          salary.innerHTML =
            "> " +
            Currency +
            value[handle].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }
      } else {
        if (number_spreater) {
          salary.innerHTML =
            Currency + value[handle].replace(/\B(?=(\d{3})+(?!\d))/g, ".");
        } else {
          salary.innerHTML =
            Currency + value[handle].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }
      }
    });
  });
  //---------------------------
  // CALCULATOR
  //---------------------------

  //initiate value
  //---------------------------
  // CALCULATOR
  //---------------------------

  // initiate value
  var mobileAmount = $("#mobile-amount"),
    mobileYear = $("#mobile-year"),
    inputAmount = $("#input-amount"),
    inputYear = $("#input-year"),
    calculatorMobile = $(".calculator--mobile");

  mobileAmount.text(Currency + inputAmount.val());
  mobileYear.text(
    inputYear.find(":selected").text() + " " + translate("years")
  );

  calculatorMobile.on("click", function(e) {
    if (
      $(this)
        .nextAll()
        .is(":hidden")
    ) {
      $(this)
        .nextAll()
        .slideDown();
      $(this)
        .addClass("calculator--mobile--active")
        .find("strong")
        .text(translate("Hide Calculator"));
    } else {
      $(this)
        .nextAll()
        .slideUp(function() {
          $(".calculator--main, .calculator--filters").removeAttr("style");
        });
      $(this)
        .removeClass("calculator--mobile--active")
        .find("strong")
        .text(translate("Show Calculator"));
    }
    e.preventDefault();
    $("html, body").animate(
      {
        scrollTop: $(this).offset().top,
      },
      400
    );
  });

  $(".btn-calculator-close").on("click", function(e) {
    $(this)
      .addClass("btn-calculator-close--active")
      .prop("disabled", true)
      .delay(1000)
      .queue(function(next) {
        calculatorMobile
          .removeClass("calculator--mobile--active")
          .find("strong")
          .text(translate("Show Calculator"));
        calculatorMobile.nextAll().slideUp(function() {
          $(".calculator--main, .calculator--filters").removeAttr("style");
        });
        $("html, body").animate(
          {
            scrollTop: calculatorMobile.offset().top,
          },
          400
        );
        $(this)
          .prop("disabled", false)
          .removeClass("btn-calculator-close--active");
        next();
      });
    e.preventDefault();
  });

  inputAmount.on("keyup", function() {
    mobileAmount.text(Currency + inputAmount.val());
  });

  inputYear.on("change", function() {
    var year = $(this)
      .find(":selected")
      .text();
    if (parseInt(year) === 1) {
      mobileYear.text(year + " " + translate("year"));
    } else {
      mobileYear.text(year + " " + translate("years"));
    }
  });

  //---------------------------
  // Show Hide more info
  //---------------------------

  var moreinfo = $(".btn-more-info"),
    time = 400;

  $("body").on("click", ".btn-more-info", function(e) {
    e.preventDefault();

    var tableColumns = $(this).parents(".table-columns");

    if ($(this).hasClass("active")) {
      $(this)
        .removeClass("active")
        .find("span:first-of-type")
        .text(translate("more"));
      tableColumns
        .removeClass("table-columns--expand")
        .siblings(".col-more-info")
        .slideUp(time);
      $("html, body").animate(
        {
          scrollTop: $(this)
            .parents(".table__product")
            .offset().top,
        },
        time
      );
    } else {
      $(this)
        .addClass("active")
        .find("span:first-of-type")
        .text(translate("less"));
      tableColumns
        .addClass("table-columns--expand")
        .siblings(".col-more-info")
        .slideDown(time);
      $("html, body").animate(
        {
          scrollTop: $(this)
            .parents(".table__product")
            .offset().top,
        },
        time
      );
    }
  });

  $("body").on("click", ".btn-close", function(e) {
    e.preventDefault();
    var thisBtn = $(this);
    var sibling = thisBtn.parents(".col-more-info").siblings(".table-columns");
    thisBtn.parents(".col-more-info").slideUp(time);
    sibling
      .removeClass("table-columns--expand")
      .find(".btn-more-info")
      .removeClass("active")
      .find("span:first-of-type")
      .text(translate("more"));
    $("html, body").animate(
      {
        scrollTop: thisBtn.parents(".table__product").offset().top,
      },
      time
    );
  });

  //---------------------------
  // Compare product
  //---------------------------
  // add bank to the Compare panel at sidebar
  function addBank(
    cross_sell,
    product_path,
    title,
    bankId,
    bankName,
    bankinterestrate,
    bankinstallment,
    bankeligibility,
    bankapprovalduration,
    bankmaximumtenure,
    bankminimumtenure,
    bankminimumincome,
    TotalAmountPayable,
    TotalInterestPayable,
    imgurl,
    minage,
    maxage,
    highlight,
    url,
    externalurl,
    maximumamount,
    minimumamount,
    applyformtype,
    jappmachinename,
    productPageUrl
  ) {
    $(
      '<div class="cp-product"  data-url-product-page="' +
        productPageUrl +
        '" data-product-path="' +
        product_path +
        '" data-cross-sell="' +
        cross_sell +
        '" data-title ="' +
        title +
        '" id="' +
        bankId +
        '" data-href="' +
        externalurl +
        '"  data-id="' +
        bankId +
        '" data-interestrate="' +
        bankinterestrate +
        '"data-installment="' +
        bankinstallment +
        '"data-highlight="' +
        highlight +
        '"data-eligibility="' +
        bankeligibility +
        '"data-bank="' +
        bankName +
        '" data-img="' +
        imgurl +
        '"data-bankminimumincome="' +
        bankminimumincome +
        '"data-minage="' +
        minage +
        '"data-maxage="' +
        maxage +
        '"data-approvalduration="' +
        bankapprovalduration +
        '"data-maximumtenure="' +
        bankmaximumtenure +
        '" data-maximumamount="' +
        maximumamount +
        '" data-minimumamount="' +
        minimumamount +
        '" data-totalamountpayable="' +
        TotalAmountPayable +
        '"data-totalinterestpayable="' +
        TotalInterestPayable +
        '" data-minimumtenure="' +
        bankminimumtenure +
        '" data-minimumincome="' +
        bankminimumincome +
        '"data-url="' +
        url +
        '"data-applyformtype="' +
        applyformtype +
        '"' +
        'data-jappmachinename="' +
        jappmachinename +
        '"  >' +
        '<div class="cp-product-name">' +
        title +
        '</div><div class="cp-product-close"><a class="close" href="#">' +
        "</a></div>"
    )
      .appendTo("#cp-product-list")
      .hide()
      .fadeIn();
    $(document.body).trigger("sticky_kit:recalc");
  }

  // remove bank to the Compare panel at sidebar
  function removeBank(bankId) {
    $("#" + bankId).remove();
    $(document.body).trigger("sticky_kit:recalc");
  }

  // calculate number of checked product
  function hasProduct() {
    var num = $("#cp-product-list").children().length;
    var btn = $(".btn-cp-product");

    btn.text(translate("Compare") + " (" + num + "/3)");

    if (num) {
      $(".cp-product--empty").hide();

      if (num > 1) {
        btn.prop("disabled", false).removeClass("btn-cp-product--disabled");
      } else if (num < 2) {
        btn.prop("disabled", true).addClass("btn-cp-product--disabled");
      }
    } else {
      $(".cp-product--empty").show();
    }
  }

  $("body").on("click", ".col-compare-btn", function() {
    var checkedBank = $(this)
      .parents(".table__product")
      .data("bank");
    var title = $(this)
      .parents(".table__product")
      .data("title");
    var checkedBankId = $(this)
      .parents(".table__product")
      .data("id");
    var checkedBankinterestrate = $(this)
      .parents(".table__product")
      .data("interestrate");
    var checkedBankinstallment = $(this)
      .parents(".table__product")
      .data("installment");
    var checkedBankeligibility = $(this)
      .parents(".table__product")
      .data("eligibility");
    var checkedBankapprovalduration = $(this)
      .parents(".table__product")
      .data("approvalduration");
    var checkedBankmaximumtenure = $(this)
      .parents(".table__product")
      .data("maximumtenure");
    var checkedBankminimumtenure = $(this)
      .parents(".table__product")
      .data("minimumtenure");
    var checkedBankminimumincome = $(this)
      .parents(".table__product")
      .data("minimumincome");
    var TotalAmountPayable = $(this)
      .parents(".table__product")
      .data("totalamountpayable");
    var TotalInterestPayable = $(this)
      .parents(".table__product")
      .data("totalinterestpayable");
    var imgurl = $(this)
      .parents(".table__product")
      .data("img");
    var minage = $(this)
      .parents(".table__product")
      .data("minage");
    var maxage = $(this)
      .parents(".table__product")
      .data("maxage");
    var highlight = $(this)
      .parents(".table__product")
      .data("highlight");
    var url = $(this)
      .parents(".table__product")
      .data("url-product-page");
    var minimumamount = $(this)
      .parents(".table__product")
      .data("minimumamount");
    var maximumamount = $(this)
      .parents(".table__product")
      .data("maximumamount");
    var externalurl = $(this)
      .parents(".table__product")
      .data("href");
    var applyformtype = $(this)
      .parents(".table__product")
      .data("applyformtype");
    var jappmachinename = $(this)
      .parents(".table__product")
      .data("jappmachinename");
    var product_path = $(this)
      .parents(".table__product")
      .data("product-path");
    var cross_sell = $(this)
      .parents(".table__product")
      .data("cross-sell");
    var productPageUrl = $(this)
      .parents(".table__product")
      .data("url-product-page");

    //if is not checked, check it
    if ($(this).hasClass("col-compare-btn--active")) {
      removeBank(checkedBankId);
      $(this).removeClass("col-compare-btn--active");
    } else {
      if ($("#cp-product-list").children().length === 3) {
        alert(translate("Max number reached. Remove one product to add this"));
      } else {
        $(this).addClass("col-compare-btn--active");
        addBank(
          cross_sell,
          product_path,
          title,
          checkedBankId,
          checkedBank,
          checkedBankinterestrate,
          checkedBankinstallment,
          checkedBankeligibility,
          checkedBankapprovalduration,
          checkedBankmaximumtenure,
          checkedBankminimumtenure,
          checkedBankminimumincome,
          TotalAmountPayable,
          TotalInterestPayable,
          imgurl,
          minage,
          maxage,
          highlight,
          url,
          externalurl,
          maximumamount,
          minimumamount,
          applyformtype,
          jappmachinename,
          productPageUrl
        );
      }
    }
    hasProduct();
  });

  $(".cp-product-list").on("click", ".cp-product a", function(e) {
    var bankId = $(this)
      .closest(".cp-product")
      .prop("id");
    removeBank(bankId);
    hasProduct();
    $('.table__product[data-id="' + bankId + '"]')
      .find(".col-compare-btn")
      .removeClass("col-compare-btn--active");
    e.preventDefault();
  });

  $("#existingcard").change(function() {
    if (!document.getElementById("existingcard").checked) {
      $("#existingcardTenureBlock").hide();
      $("#existingcardTenure").removeAttr("required");
    } else {
      $("#existingcardTenureBlock").show();
      $("#existingcardTenure").attr("required", "");
    }
  });

  //---------------------------
  // MODAL & FORM
  //---------------------------

  $(".helper--faq").on("click", function(e) {
    $("html, body").animate(
      {
        scrollTop: $("#faq").offset().top,
      },
      1000,
      "easeInOutQuad"
    );
    e.preventDefault();
  });

  // Modal Compare
  $("#btn-cp-product").on("click", function(e) {
    var checkindex;
    $(".cp-product").each(function(index) {
      checkindex = index;
      $("#product-" + index + "-applybtnup").attr(
        "data-product-path",
        $(this).attr("data-product-path")
      );
      $("#product-" + index + "-applybtndown").attr(
        "data-product-path",
        $(this).attr("data-product-path")
      );

      $("#product-" + index + "-applybtnup").attr(
        "data-id",
        $(this).data("id")
      );
      $("#product-" + index + "-applybtndown").attr(
        "data-id",
        $(this).data("id")
      );

      $("#product-" + index + "-applybtnup").attr(
        "data-url-product-page",
        $(this).data("url-product-page")
      );
      $("#product-" + index + "-applybtndown").attr(
        "data-url-product-page",
        $(this).data("url-product-page")
      );

      $("#product-" + index + "-applybtnup").attr(
        "data-cross-sell",
        $(this).attr("data-cross-sell")
      );
      $("#product-" + index + "-applybtndown").attr(
        "data-cross-sell",
        $(this).attr("data-cross-sell")
      );

      $("#product-" + index + "-applybtnup").attr(
        "data-nid",
        $(this).attr("data-id")
      );
      $("#product-" + index + "-applybtndown").attr(
        "data-nid",
        $(this).attr("data-id")
      );

      $("#product-" + index + "-applybtnup").attr(
        "data-href",
        $(this).attr("data-href")
      );
      $("#product-" + index + "-applybtndown").attr(
        "data-href",
        $(this).attr("data-href")
      );

      $("#product-" + index + "-applybtnup").attr(
        "href",
        $(this).attr("data-href")
      );
      $("#product-" + index + "-applybtndown").attr(
        "href",
        $(this).attr("data-href")
      );

      $("#product-" + index + "-name").html($(this).data("title"));
      $("#product-" + index + "-applybtnup").attr(
        "data-minimumamount",
        $(this).data("minimumamount")
      );
      $("#product-" + index + "-applybtnup").attr(
        "data-maximumamount",
        $(this).data("maximumamount")
      );

      $("#product-" + index + "-applybtndown").attr(
        "data-minimumamount",
        $(this).data("minimumamount")
      );
      $("#product-" + index + "-applybtndown").attr(
        "data-maximumamount",
        $(this).data("maximumamount")
      );

      $("#product-" + index + "-name").attr("href", "/" + $(this).data("url"));
      $("#product-" + index + "-Interest").html(
        $(this).data("interestrate") + "%"
      );
      $("#product-" + index + "-repayment").html(
        Currency + $(this).data("installment")
      );
      $("#product-" + index + "-payable").html(
        $(this).data(Currency + "totalamountpayable")
      );
      $("#product-" + index + "-paid").html(
        $(this).data(Currency + "totalinterestpayable")
      );
      $("#product-" + index + "-approval").html(
        $(this).data("approvalduration")
      );
      $("#product-" + index + "-empreq").html($(this).data("eligibility"));
      $("#product-" + index + "-img").attr("src", $(this).data("img"));
      $("#product-" + index + "-Age").html(
        $(this).data("minage") + " - " + $(this).data("maxage")
      );
      $("#product-" + index + "-salary").html(
        Currency +
          $(this).data("bankminimumincome") +
          " " +
          translate("gross per month")
      );
      $("#product-" + index + "-Dis").html($(this).data("highlight"));
      //$("#product-"+index+"-salary").html($(this).data('eligibility'));
      $("#product-" + index + "-payable").html(
        Currency + $(this).data("totalamountpayable")
      );
      $("#product-" + index + "-paid").html(
        Currency + $(this).data("totalinterestpayable")
      );
      $("#product-" + index + "-tenure").html(
        $(this).data("minimumtenure") +
          " to " +
          $(this).data("maximumtenure") +
          " " +
          translate("years")
      );
      $("#product-" + index + "-applybtnup").attr({
        "data-bank": $(this).data("bank"),
        "data-title": $(this).data("title"),
        "data-vertical": "Personal Loan",
        "data-img": $(this).data("img"),
        "data-interestrate": $(this).data("interestrate"),
        "data-applyformtype": $(this).attr("data-applyformtype"),
        "data-jappmachinename": $(this).attr("data-jappmachinename"),
      });
      $("#product-" + index + "-applybtndown").attr({
        "data-bank": $(this).data("bank"),
        "data-title": $(this).data("title"),
        "data-vertical": "Personal Loan",
        "data-img": $(this).data("img"),
        "data-interestrate": $(this).data("interestrate"),
        "data-applyformtype": $(this).attr("data-applyformtype"),
        "data-jappmachinename": $(this).attr("data-jappmachinename"),
      });
    });

    if (checkindex == 1) {
      $("#product-2-name").html("");
      $("#product-2-Interest").html("");
      $("#product-2-repayment").html("");
      $("#product-2-payable").html("");
      $("#product-2-paid").html("");
      $("#product-2-approval").html("");
      $("#product-2-img").attr("src", "");
      $("#product-2-Age").html("");
      $("#product-2-salary").html("");
      $("#product-2-Dis").html("");
      $("#product-2-empreq").html("");
      $("#product-2-tenure").html("");
      $("#product-2-apply").hide();
      $("#product-2-apply2").hide();
    } else {
      $("#product-2-apply").show();
      $("#product-2-apply2").show();
    }

    $("#modal-compare").modal({
      show: true,
    });
    e.preventDefault();
  });

  //fixed unassigned body class (modal-open) when there are more modal opened
  $(".modal").on("hidden.bs.modal", function(e) {
    if ($(".modal:visible").length) {
      $("body").addClass("modal-open");
    }
  });

  $("#modal-apply").on("hidden.bs.modal", function() {
    $("#applyForm")
      .parsley()
      .reset();
    $("#applyForm")
      .parsley()
      .destroy();
    $("#identification_no").val("");
  });

  if (region == "my") {
    $("body").on("click", ".btn-apply", function(e) {
      if ((["2964", "2961", "2962","2965" ].indexOf($(this).attr("data-id")) >= 0)) {
        window.open(
            $(this).attr("data-href")
          );
      } else {
        e.preventDefault();
        window.open(
          "/" + $(this).attr("data-url-product-page") + "?apply=true",
          "_blank" // <- This is what makes it open in a new window.
        );
      }
    });
  } else {
    // Modal apply
    $("body").on("click", ".btn-apply", function(e) {
      e.preventDefault();
      var that = $(this);
      if (region == "id" && $(this).attr("data-href")) {
        $("#img-popup-tunaiku").attr("src", $(this).attr("data-img"));
        $("#bank-name-popup-tunaiku").html($(this).attr("data-title"));

        $("#tunaiku-btn-pl-submit").attr(
          "data-href",
          $(this).attr("data-href")
        );
        $("#tunaiku-btn-pl-submit").attr("data-nid", $(this).attr("data-id"));
        $("#tunaiku-btn-pl-submit").attr(
          "data-value",
          $(this).attr("data-value")
        );
        // $("#tunaiku-btn-pl-submit").attr("data-img", $(this).attr('data-title'));
        $("#modal-tunaiku-apply").modal({
          show: true,
        });
        return 0;
      }

      $("#btn-pl-submit").attr("data-nid", $(this).data("nid"));
      if (
        typeof $(this).attr("data-href") === "undefined" ||
        $(this).attr("data-href") == "undefined" ||
        region == "ph"
      ) {
        $("#img-popup").attr("src", $(this).attr("data-img"));
        if (region == "id") {
          document.getElementById("identification_no").style.display = "block";
          // document.getElementById("identification_no").required = true;
        } else {
          document.getElementById("identification_no").style.display = "none";
          //$('#identification_no').removeAttr('required');
          //document.getElementById("identification_no").required = false;
          $("#identification_no").removeAttr("required");
        }

        if ($(this).data("nid") == "746") {
          document.getElementById("location").style.display = "block";
          document.getElementById("existingcardText").innerHTML = translate(
            "Do you have an existing savings account?"
          );

          // document.getElementById("existingcardBlock").style.display = "none";
        } else {
          if (region != "sg") {
            document.getElementById("existingcardText").innerHTML = translate(
              "Do you have an existing credit card?"
            );
            document.getElementById("location").style.display = "none";
            document.getElementById("existingcardBlock").style.display =
              "block";
            $("#locationInput").removeAttr("required");
          }
        }
        $("#btn-pl-submit").attr("data-title", $(this).data("title"));
        $("#btn-pl-submit").attr("data-value", $(this).data("title"));
        $("#btn-pl-submit").attr("data-bank", $(this).data("bank"));
        $("#btn-pl-submit").attr(
          "data-islamic_conventional",
          $(this).data("islamic_conventional")
        );
        $("#btn-pl-submit").attr("data-maxirat", $(this).data("maxirat"));
        $("#btn-pl-submit").attr("data-minirate", $(this).data("minirate"));
        $("#btn-pl-submit").attr(
          "data-interestrate",
          $(this).data("interestrate")
        );
        $("#btn-pl-submit").removeAttr("data-href");
        $("#btn-pl-submit").attr("data-href", $(this).attr("data-href"));

        $("#apply-amount").attr("min", $(this).data("minimumamount"));
        $("#apply-amount").attr("max", $(this).data("maximumamount"));
        $("#apply-amount").attr(
          "data-parsley-min",
          $(this).data("minimumamount")
        );
        $("#apply-amount").attr(
          "data-parsley-max",
          $(this).data("maximumamount")
        );
        $("#apply-amount").attr(
          "data-parsley-error-message",
          translate("The amount should be between ") +
            Currency +
            $(this).data("minimumamount") +
            translate(" to ") +
            Currency +
            $(this).data("maximumamount")
        );
        $("#apply-income").attr("min", $(this).data("minimumincome"));
        $("#apply-income").attr(
          "data-parsley-min",
          $(this).data("minimumincome")
        );
        $("#btn-pl-submit").attr("data-value", $(this).data("bank"));
        $("#btn-pl-submit").attr("data-nid", $(this).attr("data-id"));
        $("#btn-pl-submit").attr(
          "data-jappmachinename",
          $(this).attr("data-jappmachinename")
        );
        $("#btn-pl-submit").attr(
          "data-applyformtype",
          $(this).attr("data-applyformtype")
        );
        $("#apply-amount").attr(
          "data-parsley-min-message",
          translate("Min. amount for this bank is ") +
            Currency +
            $(this).data("minimumamount")
        );
        $("#apply-amount").attr(
          "data-parsley-max-message",
          translate("Max. amount is ") +
            Currency +
            $(this).data("maximumamount")
        );
        //$("#apply-amount").attr("data-parsley-errors-container",$(this).data('img'));

        $(".apply-product__interest").html($(this).data("interestrate") + "%");
        $("#bank-name-popup").html($(this).data("bank"));
        $("#modal-apply").modal({
          show: true,
        });
      } else {
        window.open(
          $(this).attr("data-href"),
          "_blank" // <- This is what makes it open in a new window.
        );
        $(".modal-footer .btn-continue span").text($(this).data("title"));
        $(".modal-footer .btn-continue").attr(
          "data-value",
          $(this).data("title")
        );
        $(".modal-footer .btn-continue").attr(
          "href",
          $(this).attr("data-href")
        );
      }

      e.preventDefault();
    });
  }

  $("#tunaiku-btn-pl-submit").click(function() {
    var that = $(this);
    var nid = that.attr("data-nid");
    if (
      $("#tunaikuApplyForm")
        .parsley()
        .isValid()
    ) {
      $("#tunaiku-btn-pl-submit").html(translate("Submitting.."));
      new CampaignFactory(region, undefined, function(factory) {
        var cam = factory.getByNid(that.attr("data-nid"));
        cam.setFieldValue("name", document.getElementById("tunaikuName").value);
        cam.setFieldValue(
          "phone_number",
          document.getElementById("tunaikuPhone").value
        );
        cam.setFieldValue(
          "email_address",
          document.getElementById("tunaikuEmail").value
        );
        if (region == "id") {
          cam.setFieldValue(
            "city",
            document.getElementById("tunaikuLocationInput").value
          );
        }
        cam.submit(function(result, status, params, callback_params) {
          if (status == "complete") {
            window.location.href = that.attr("data-href");
          }
        });
      });
    }
  });

  var radioValue;
  $("input[type='radio']").click(function() {
    radioValue = $("input[name='existingcardTenure']:checked").val();
  });

  $("#btn-pl-submit").click(function() {
    if (
      $("#applyForm")
        .parsley()
        .isValid()
    ) {
      $("#btn-pl-submit").html(translate("Submitting.."));
      new CampaignFactory(region, undefined, function(factory) {
        IMDevPropBag.add("SRCREGION", lang + "_pl_listing");
        var cam;
        if ($("#btn-pl-submit").attr("data-applyformtype") == "2") {
          cam = factory.getByMachineName(
            $("#btn-pl-submit").attr("data-jappmachinename")
          );
        } else {
          cam = factory.getByNid($("#btn-pl-submit").attr("data-nid"));
        }
        cam.setFieldValue("name", document.getElementById("name").value);
        cam.setFieldValue(
          "phone_number",
          document.getElementById("phone").value
        );
        cam.setFieldValue(
          "email_address",
          document.getElementById("email").value
        );
        cam.setFieldValue(
          "loan_amount",
          document.getElementById("apply-amount").value
        );
        cam.setFieldValue(
          "loan_tenure",
          document.getElementById("input-year").value
        );
        cam.setFieldValue(
          "monthly_salary",
          document.getElementById("apply-income").value
        );
        if ($(this).data("applyformtype") != null) {
          cam.setFieldValue(
            "identification_no",
            document.getElementById("identification_no").value
          );
          cam.setFieldValue(
            "productname",
            $("#btn-pl-submit").attr("data-value")
          );
        }

        if (region == "id") {
          cam.setFieldValue(
            "identification_no",
            document.getElementById("identification_no").value
          );
        }
        if (angular.element($("#input-amount")).scope().government)
          cam.setFieldValue("is_government_servant", "yes");
        else cam.setFieldValue("is_government_servant", "no");
        // new fields
        try {
          cam.setFieldValue("city", document.getElementById("city").value);
          cam.setFieldValue("gender", document.getElementById("gender").value);
        } catch (e) {}
        try {
          if (document.getElementById("no-npwp").checked)
            cam.setFieldValue("npwp_no", "yes");
          else cam.setFieldValue("npwp_no", "no");
        } catch (e) {}
        try {
          if (document.getElementById("existingcard").checked)
            cam.setFieldValue("have_another_credit_card", "yes");
          else cam.setFieldValue("have_another_credit_card", "no");

          if (radioValue) {
            cam.setFieldValue("another_credit_card_period", radioValue);
          }
        } catch (e) {}

        if ($("#btn-pl-submit").attr("data-nid") == "746") {
          cam.setFieldValue(
            "resident_work_location",
            document.getElementById("locationInput").value
          );
          try {
            if (document.getElementById("existingcard").checked)
              cam.setFieldValue("have_another_savings_account", "Yes");
            else cam.setFieldValue("have_another_savings_account", "No");
          } catch (e) {}
        }
        cam.submit(function(result, status, params, callback_params) {
          if (status == "complete") {
            if (region == "ph") {
              window.location.href = $("#btn-pl-submit").attr("data-href")
                ? $("#btn-pl-submit").attr("data-href")
                : thank_you;
            } else window.location.href = thank_you;
          }
        });
      });
    }
  });

  $("#btn-enquire-submit").click(function() {
    if (
      $("#form-enquire")
        .parsley()
        .isValid()
    ) {
      $("#btn-enquire-submit").html(translate("Submitting.."));
      new CampaignFactory(region, undefined, function(factory) {
        var cam = factory.getByMachineName(
          region.toUpperCase() + "_PL_iMoney_iMoney_PL_Call_Me_Back"
        );
        cam.setFieldValue(
          "name",
          document.getElementById("enquire-name").value
        );
        cam.setFieldValue(
          "phone_number",
          document.getElementById("enquire-phone").value
        );
        cam.setFieldValue(
          "email_address",
          document.getElementById("enquire-email").value
        );
        cam.setFieldValue("enquiry", $("#enquire-question").val());

        cam.submit(function(result, status, params, callback_params) {
          if (status == "complete") {
            $("#btn-enquire-submit").html(translate("Submitted"));
          }
        });
      });
      //return false;
    }
  });

  $("#government-servant").on("click", function(e) {
    var scope = angular.element($("#government-servant")).scope().government;
    var imuTrack = new IMUTrack({
      region: "Listing Filter",
      subject: "User",
      action: "Input",
      description: "government servant",
      vertical: "Personal Loan",
      value: scope,
    });
    userManager.track(imuTrack);
  });

  $("#apply-government-servant").on("click", function(e) {
    var scope = angular.element($("#government-servant")).scope().government;
    var imuTrack = new IMUTrack({
      region: "Listing Filter popup",
      subject: "User",
      action: "Input",
      description: "government servant",
      vertical: "Personal Loan",
      value: scope,
    });
    userManager.track(imuTrack);
  });

  $("#islamic-only").on("click", function(e) {
    var scope = angular.element($("#islamic-only")).scope().islamic;
    var imuTrack = new IMUTrack({
      region: "Listing Filter",
      subject: "User",
      action: "Input",
      description: "Islamic Loan",
      vertical: "Personal Loan",
      value: scope,
    });
    userManager.track(imuTrack);
  });
  // Modal How
  $(".helper--how").on("click", function(e) {
    $("#modal-how").modal({
      show: true,
    });
    e.preventDefault();
  });

  // Modal Enquire
  $(".helper--ask").on("click", function(e) {
    $("#modal-enquire").modal({
      show: true,
    });
    e.preventDefault();
  });

  $("#modal-enquire").on("hidden.bs.modal", function() {
    $(this)
      .find(".modal-form")
      .show()
      .next()
      .hide()
      .removeClass("is-visible");
    $(".enquire-bee").removeClass("enquire-bee--success");
  });

  // enquiry submit on success
  $("#form-enquire").on("submit", function(e) {
    $(this)
      .closest(".modal-form")
      .hide()
      .next()
      .fadeIn();
    $(".icon-success").addClass("icon-success-in");
    $(".modal-success").addClass("is-visible");
    $(".enquire-bee").addClass("enquire-bee--success");
    e.preventDefault();
  });

  $("#existingcard").click();

  // StickyKit
  $(".section-faq .helper").stick_in_parent({
    offset_top: 30,
  });

  $(".compare-products").stick_in_parent({
    parent: ".comparison-side",
    offset_top: 10,
  });
});
