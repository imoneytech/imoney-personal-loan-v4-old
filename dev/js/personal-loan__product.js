// @codekit-prepend "rangeslider/rangeslider.js";
// @codekit-prepend "../../bower_components/imoney-assets/bower_components/nouislider/distribute/nouislider.min.js";
// @codekit-prepend "../../bower_components/imoney-assets/bower_components/nouislider/documentation/assets/wNumb.js";
// @codekit-prepend "../../bower_components/imoney-assets/bower_components/bootstrap-sass/assets/javascripts/bootstrap/transition.js";
// @codekit-prepend "../../bower_components/imoney-assets/bower_components/bootstrap-sass/assets/javascripts/bootstrap/modal.js";
// @codekit-prepend "../../bower_components/imoney-assets/bower_components/slick.js/slick/slick.min.js

$(document).ready(function () {
    //---------------------------
    // NOUISLIDER
    //---------------------------
    var slider = document.getElementById('slider-salary');
    var salary = document.getElementById('salary-howmuch');
    var salvalue = DefaultMonthly_Salary;
    var tenureval = tenure;
    var amountval = loan_amount;

    noUiSlider.create(slider, {
        start: parseInt(Max_value_Salary / 2),
        animate: true,
        step: Step_salary,
        range: {
            min: Math.ceil(Loan.minimumIncome / 100) * 100,
            max: Max_value_Salary
        },
        format: wNumb({
            decimals: 0
        })
    });

    $('#existingcard').change(function () {
        if(!document.getElementById('existingcard').checked){
            $("#existingcardTenureBlock" ).hide();
            $("#existingcardTenure" ).removeAttr('required');
        }else{
            $("#existingcardTenureBlock" ).show();
            $('#existingcardTenure').attr('required', '');
        }
    });

    var radioValue;
    $("input[type='radio']").click(function(){
        radioValue = $("input[name='existingcardTenure']:checked").val();
        
    });

    if(region!='sg'){
        $(".btn-apply-complement").click(function(){
            $('#modal-apply').modal();
        });
    }

    function submitToJApp() {
        var radioValue;
        $("input[type='radio']").click(function(){
            radioValue = $("input[name='existingcardTenure']:checked").val();            
        });
        $('#apply-now-btn').on('click', function (e) {
            e.preventDefault();
            $('#form-apply').parsley().validate();
            if ($('#form-apply').parsley().isValid()) {
                var $buttonProceed = $(this);
                $buttonProceed.html(translate('Please wait...')).attr('disabled', 'disabled');
                if ($('#form-apply').parsley().isValid()) {
                    $buttonProceed.html(translate("Submitting.."));
                    IMDevPropBag.add('SRCREGION', lang + '_pl_product');
                    new CampaignFactory(region, undefined, function (factory) {
                        var cam;
                        if (Loan.applyFormType == "2") {
                            cam = factory.getByMachineName(Loan.jappMachineName);
                            cam.setFieldValue('identification_no', document.getElementById('identification_no').value);
                            cam.setFieldValue('productname', Loan.title);
                        } else {
                            cam = factory.getByNid(Loan.nid);
                        }
                        if (region == 'id') {
                            cam.setFieldValue('identification_no', document.getElementById('identification_no').value);
                        }
                        cam.setFieldValue('name', document.getElementById('apply-name').value);
                        cam.setFieldValue('phone_number', document.getElementById('apply-phone').value);
                        cam.setFieldValue('email_address', document.getElementById('apply-email').value);
                        cam.setFieldValue('loan_amount', document.getElementById('apply-amount').value);
                        cam.setFieldValue('loan_tenure', document.getElementById('apply-tenure').value);
                        /* Leo hotfix.*/
                        //tempSalary = document.getElementById('apply-income') ? document.getElementById('apply-income').value : salvalue;
                        try {
                            cam.setFieldValue('monthly_salary', document.getElementById('apply-income').value);
                        } catch (e) { }
                        /* cam.setFieldValue('monthly_salary', document.getElementById('apply-income').value); */
                        /* End Leo hotfix */
                        if (Loan.governmentOnly == "Yes")
                            cam.setFieldValue('is_government_servant', "yes");
                        else cam.setFieldValue('is_government_servant', "no");
                        try {
                            cam.setFieldValue('gender', document.getElementById('gender').value);
                            cam.setFieldValue('city', document.getElementById('city').value);
                        } catch (e) { }
                        try {
                            if (document.getElementById('no-npwp').checked)
                                cam.setFieldValue('npwp_no', "Yes");
                            else cam.setFieldValue('npwp_no', "No");
                        } catch (e) { }


                        if (Loan.nid == '746') {
                            if (document.getElementById('existingcard').checked)
                                cam.setFieldValue('have_another_savings_account', "Yes");
                            else cam.setFieldValue('have_another_savings_account', "No");
                            // have_another_savings_account
                            cam.setFieldValue('resident_work_location', document.getElementById('locationInput').value);
                        }
                        else {
                            try {
                                if (document.getElementById('existingcard').checked)
                                    cam.setFieldValue('have_another_credit_card', "Yes");
                                else cam.setFieldValue('have_another_credit_card', "No");
                            } catch (e) { }
                        }
                        if(region=='ph'){
                            if(radioValue){
                                cam.setFieldValue('another_credit_card_period', radioValue);
                            }
                        }
                        cam.submit(function (result, status, params, callback_params) {
                            if (status == 'complete') {
                                if(region=='my'){
                                    if(Loan.field_apply_url){
                                        window.location.href = Loan.field_apply_url;
                                    }
                                    else{
                                        window.location.href = thank_you;
                                    }
                                }
                                else window.location.href = thank_you;
                            }
                        });
                    });
                }
            }
        });

    }

    slider.noUiSlider.on('change', function (value, handle) {
        var imuTrack = new IMUTrack({
            region: "Product Loan Calculator",
            subject: "User",
            action: "Input",
            description: "Salary",
            vertical: "Personal Loan",
            value: value[handle]
        });
        userManager.track(imuTrack);
    });

    slider.noUiSlider.on('update', function (value, handle) {
        salvalue = value[handle];


        var scope = angular.element($("#input-amount")).scope();
        scope.$apply(function () {
            scope.setSalary(value[handle]);
        });


        CalculationOutput(Loan, amountval, tenureval, salvalue);
        if (value == Max_value_Salary) {
            if (number_spreater) {
                salary.innerHTML = '> ' + Currency + value[handle].replace(/\B(?=(\d{3})+(?!\d))/g, ".");
            } else {
                salary.innerHTML = '> ' + Currency + value[handle].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            }
        } else {
            if (number_spreater) {
                salary.innerHTML = Currency + value[handle].replace(/\B(?=(\d{3})+(?!\d))/g, ".");
            } else {
                salary.innerHTML = Currency + value[handle].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            }
        }
    });
    CalculationOutput(Loan, amountval, tenureval, salvalue)

    function CalculationOutput(Loan, amountval, tenureval, salaryval) {
        var data = angular.injector(['ng', 'PersonalLoanApp']).get("personalLoanInterestManager").calculation(Loan, amountval, salaryval, tenureval, null);

        localStorage.setItem("interestRate", JSON.stringify(data.interestRate));
        $("#interestRate, .apply-details-interestrate").html(data.interestRate + "%");
        $("#installment, .apply-details-repayment").html(Currency + data.installment);
        $("#TotalAmountPayable").html(Currency + data.TotalAmountPayable);
        $("#TotalInterestPayable").html(Currency + data.TotalInterestPayable);
    }
    $('#input-amount').on('input', function (e) {
        amountval = this.value;
        CalculationOutput(Loan, amountval, tenureval, salvalue);
    });

    $("#input-year").change(function () {
        tenureval = this.value;
        CalculationOutput(Loan, amountval, tenureval, salvalue);
    });

    //get and clone to duplicate it
    $('.header__product-image img').clone().appendTo('.sticky--img');
    $('.apply-button .btn-apply-complement').clone().appendTo('.sticky--btn');

    var productName = $('.product-name').text();
    $('.sticky--name').text(productName);

    // var cloneNameAndImage = $('.header__product-image img, .product-name').clone();
    // $(cloneNameAndImage).appendTo('.apply-product__content');


    //make sticky product header appears when scrolling below black area
    function FeaturePosition(className) {
        var featurePosition = $(className).offset();
        return featurePosition + $(className).outerHeight();
    }

    //get the position at start
    var posFeatures = FeaturePosition('.section-calculator');

    //recalculate the position on window resize
    $(window).resize(function () {
        posFeatures = FeaturePosition('section-calculator');
        textMobile('.step1 p');
    });

    //activate the sticky header on body scroll
    $(window).scroll(function () {

        var scroll = $(window).scrollTop();

        if (scroll >= posFeatures) {
            $('.sticky-product-header').addClass('slidein');
        } else {
            $('.sticky-product-header').removeClass('slidein');
        }

    });



    //---------------------------
    // APPLICATION FORM
    //---------------------------

    function getQueryVariable(variable) {
        var query = window.location.search.substring(1);
        var vars = query.split('&');
        for (var i = 0; i < vars.length; i++) {
            var pair = vars[i].split('=');
            if (decodeURIComponent(pair[0]) == variable) {
                return decodeURIComponent(pair[1]);
            }
        }
    }
    if (!$('.btn-apply-complement').data('href')) {
        //get apply = true parameter if exist show modal
        var applyExist = getQueryVariable('apply');
        if (applyExist === 'true') {
            $('#modal-apply').modal('show');
            iosCaretFixFadeOut();
        }

        //iOS caret bug fix for 11.2 and below
        //on modal show
        $('.modal').on("show.bs.modal", function (e) {
            iosCaretFixFadeOut();
        });
        //on modal hide
        $('.modal').on("hide.bs.modal", function (e) {
            iosCaretFixFadeIn();
        });
        //function to hide body elements
        function iosCaretFixFadeOut() {
            var browserWidth = $(window).width();
            if (browserWidth <= 595) {
                $('#top, footer, .navbar, .main, .footer, .mb-navbar').fadeOut(100);
                $('.modal').css('position', 'fixed');
                $('.modal').css('overflow', 'auto');
            }
        }
        //function to show body elements
        function iosCaretFixFadeIn() {
            var browserWidth = $(window).width();
            if (browserWidth <= 595) {
                $('#top, footer, .navbar, .main, .footer, .mb-navbar').fadeIn(100);
                $('.modal').css('position', 'initial');
                $('.modal').css('overflow', 'initial');
            }
        }
        //end caret bug fix//
        //activate the form modal on btn-apply click
        if (!$('.btn-apply-complement').data('href')) {
            $('.btn-apply-complement').on('click', function (e) {
                e.preventDefault();
                $('#modal-apply').modal();
                //amountAndTenure();
            });
        } else {
            $('.btn-apply-complement').attr('href', $('.btn-apply-complement').data('href'));
        };

        //get apply = true parameter if exist show modal
        var applyExist = getQueryVariable('apply');

        if (applyExist === 'true') {
            $('#modal-apply').modal();
            //amountAndTenure();
        }

        $('#apply-income').bind('input', function () {
            salvalue = $(this).val();
        });

        // --------------------------------------
        // FOR NON-MY (without pre-qualifying form)
        // --------------------------------------
        submitToJApp()
    } else {
        if (region == "id" || region=='ph') {
            var applyExist = getQueryVariable('apply');
            if (applyExist === 'true') {
                $('#modal-apply').modal('show');
            }
            $('.btn-apply-complement').on('click', function (e) {
                e.preventDefault();
                $('#modal-apply').modal();
                //amountAndTenure();
            });
            $('#apply-now-btn').on('click', function (e) {
                e.preventDefault();
                $('#form-apply').parsley().validate();
                if ($('#form-apply').parsley().isValid()) {
                    var $buttonProceed = $(this);
                    $buttonProceed.html(translate('Please wait...')).attr('disabled', 'disabled');
                if ($('#form-apply').parsley().isValid()) {
                    $buttonProceed.html(translate("Submitting.."));
                    IMDevPropBag.add('SRCREGION', lang + '_pl_product');
                    new CampaignFactory(region, undefined, function (factory) {
                        var cam;
                        if (Loan.jappMachineName) {
                            cam = factory.getByMachineName(Loan.jappMachineName);
                        } else {
                            cam = factory.getByNid(Loan.nid);
                        }

                        cam.setFieldValue('name', document.getElementById('apply-name').value);
                        cam.setFieldValue('phone_number', document.getElementById('apply-phone').value);
                        cam.setFieldValue('email_address', document.getElementById('apply-email').value);
                        if(region=='id'){
                            cam.setFieldValue('city', document.getElementById('city').value);
                        }
                        else{
                            cam.setFieldValue('loan_amount', document.getElementById('apply-amount').value);
                            cam.setFieldValue('loan_tenure', document.getElementById('apply-tenure').value);
                            
                            if (Loan.nid == '746') {
                                if (document.getElementById('existingcard').checked)
                                    cam.setFieldValue('have_another_savings_account', "Yes");
                                else cam.setFieldValue('have_another_savings_account', "No");
                                // have_another_savings_account
                                cam.setFieldValue('resident_work_location', document.getElementById('locationInput').value);
                            }
                            else {
                                try {
                                    if (document.getElementById('existingcard').checked)
                                        cam.setFieldValue('have_another_credit_card', "Yes");
                                    else cam.setFieldValue('have_another_credit_card', "No");

                                    if(region=='ph'){
                                        if(radioValue){
                                            cam.setFieldValue('another_credit_card_period', radioValue);
                                        }
                                    }
                                } catch (e) { }
                            }
                        }

                        cam.submit(function (result, status, params, callback_params) {
                            if (status == 'complete') {
                                window.location.href = Loan.field_apply_url;
                            }
                        });
                    });
                }
                }
            });
        }
        else {
            if(region =="my"){
                // $('.btn-apply-complement').attr('href', $('.btn-apply-complement').data('href'));
                var applyExist = getQueryVariable('apply');
                if (applyExist === 'true') {
                    $('#modal-apply').modal('show');
                    // iosCaretFixFadeOut();
                }
                submitToJApp();
            }
            else $('.btn-apply-complement').attr('href', $('.btn-apply-complement').data('href'));
        }
    }

    
    //function to change the text of p elements for mobile
    var textMobile = function (element) {
        var mq = window.matchMedia("(max-width:480px)");

        if (mq.matches) {
            return $(element).text(translate('Please provide your details to begin the application process.'));
        } else {
            return $(element).text(translate('Please provide your name, email and phone number so we can contact you to begin the application process.'));
        }
    };
    $("#existingcard").click();

    if ($('body').hasClass('imoney-' + region)) {
        textMobile(translate('.step1 p'));
    }
});