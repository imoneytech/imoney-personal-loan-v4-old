function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

$(document).ready(function(){
	var customer_name = getParameterByName('customer_name');
	var state_index = getParameterByName('state_index');
	var outlet_index = getParameterByName('outlet_index');
	
	$("#customer_name").text(customer_name);
	$("#state").text(easyJson.states[state_index].name);
	$("#outlet").text(easyJson.states[state_index].outlets[outlet_index].easyName);
	$('#outletAddress').text(easyJson.states[state_index].outlets[outlet_index].easyAddress);
})