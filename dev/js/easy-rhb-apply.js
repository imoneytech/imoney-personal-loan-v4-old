// @codekit-append "../../bower_components/imoney-assets/bower_components/chosen/chosen.jquery.min.js";
// @codekit-append "../../bower_components/imoney-assets/bower_components/slick.js/slick/slick.min.js";

$(document).ready(function(){

	//cta button top
	$('.btn-apply-top').on('click',function(e){
		e.preventDefault();

		$('html, body').animate({
			scrollTop : $('.form-heading').offset().top - 20
		})
	})

	//animate selectbox if disabled apply btn is clicked
	$('.submit-wrapper').on('click',function(){
		$this = $(this);

		if($this.find('button').is(':disabled')){
			$this.parent('.location-wrapper').children('.form-group').find('input[type="checkbox"], .clarify').addClass('animated pulse').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
				$this.parent('.location-wrapper').children('.form-group').find('input[type="checkbox"], .clarify').removeClass('animated pulse');
			});
		}
	})

		//animate selectbox if disabled apply btn is clicked
		$('.location-wrapper .form-group:nth-child(2)').on('click',function(){
			$this = $(this);
		//console.log('click');
		if($this.find('select').is(':disabled') || $this.children('.chosen-container').hasClass('.chosen-disabled')){
			$this.parent('.location-wrapper').children('.form-group:first-child').find('select, .chosen-container').addClass('animated pulse').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
				$this.parent('.location-wrapper').children('.form-group:first-child').find('select, .chosen-container').removeClass('animated pulse');
			});
		}
	})

		function getQueryVariable(variable) {
			var query = window.location.search.substring(1);
			var vars = query.split('&');
			for (var i = 0; i < vars.length; i++) {
				var pair = vars[i].split('=');
				if (decodeURIComponent(pair[0]) == variable) {
					return decodeURIComponent(pair[1]);
				}
			}
   //console.log('Query variable %s not found', variable);
 }

 function getParameterByName(name) {
 	name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
 	var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
 	results = regex.exec(location.search);
 	return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
 }

 var src_region = getParameterByName('src');

 var utm_source = getQueryVariable('utm_source') ? getQueryVariable('utm_source'): '';
 var utm_medium = getQueryVariable('utm_medium') ? getQueryVariable('utm_medium'): '';
 var utm_term = getQueryVariable('utm_term') ? getQueryVariable('utm_term'): '';
 var utm_content = getQueryVariable('utm_content') ? getQueryVariable('utm_content'): '';
 var utm_campaign = getQueryVariable('utm_campaign') ? getQueryVariable('utm_campaign'): '';

	//Disable scrolling for Input type number
	$('#rhb-financing-form').on('focus', 'input[type=number]', function (e) {
		$(this).on('mousewheel.disableScroll', function (e) {
			e.preventDefault()
		})
	})

	$('#rhb-financing-form').on('blur', 'input[type=number]', function (e) {
		$(this).off('mousewheel.disableScroll')
	})				

	//Form submit function
	$('#rhb-financing-form').submit(function(e){
		e.preventDefault();
		var linkFinish = "www.google.com";


		var dataPost = {
			ajax : 'true',
			email_address : jQuery('#email').val(),
			apikey : "b43fd3fb4d6ffc1b3470593e3e6a7b16-us5",
			id : "062fc5ed27",
			output : "json",
			double_optin : "false",
			update_existing : 'true',
			merge_vars : {
				'campaign_id' : '54c60ab6daaf45b2358b456c',
				'name' : $('input[name="name"]').val(),
				'email_address' : $('input[name="email"]').val(),
				'phone_number' : $('input[name="mobile-num"]').val(),
				'identification_no' : $('input[name="ic-number"]').val(),
				'loan_amount' : $('input[name="amount"]').val(),
				'branch_state' : $('#state').val(),
				'Branch' : $('#rhb-outlet').val(),
				'UTSOURCE' : utm_source,
				'UTMEDIUM' : utm_medium,
				'UTTERM' : utm_term,
				'UTCONTENT' : utm_content,
				'UTCAMPAIGN' : utm_campaign,
				'GROUPINGS' : [{name: 'Source', 'groups' : 'CreditCheck'}]
			}
		};



		$('#rhb-btn-submit').text('Sending..').addClass('waiting').prop('disabled', true);
		

		$.ajax({
			url: 'https:' == document.location.protocol ? 'https://apply.imoney.my/api/save/submission?src='+src_region : 'http://dev.formapp.ly/api/save/submission?src='+src_region,
			//url: 'http://dev.formapp.ly/api/save/submission',
			type: "POST",
			async: true,
			jsonpCallback: 'callback',
			contentType: "application/json",
			data: dataPost,
			dataType: 'jsonp',
			success: function(data)
			{
				if(data.success){
					var selectedIndexState = $('#state')[0].selectedIndex-1;
					var selectedIndexRhb = $('#rhb-outlet')[0].selectedIndex-1;
					window.location = '/personal-loan/rhb-easy-pinjaman-ekspres/thank-you?customer_name='+$('input[name="name"]').val()+"&state_index="+selectedIndexState+"&outlet_index="+selectedIndexRhb ;
					console.log(data);
				} else {
					alert(data.data);
				}
			},
			error: function()
			{
				console.error('failed');
				alert('Error Sending Data');
			}
		});
	});

	//populate easy outlet

	$('#outlet-more-info').hide();

	$('#state').on('change',function(){

		if($(this).val() !== ""){
			//$('#rhb-btn-submit').prop('disabled',false);
			$('#rhb-outlet').prop('disabled', false).trigger('chosen:updated');
		}
		else {
			//$('#rhb-btn-submit').prop('disabled',true);
			$('#rhb-outlet').prop('disabled', true).trigger('chosen:updated');
		}

		var thisStateName = $(this).val();

		$('#rhb-outlet').empty().trigger('chosen:updated');
		$('#outlet-more-info').fadeOut();

		for(var i = 0; i < easyJson.states.length; i++){
			//console.log('test');
			if(easyJson.states[i].name ==  thisStateName){
				// console.log(thisStateName);
				$('#rhb-outlet').html('<option value="">Choose ' +thisStateName+ ' Easy Outlet</option>').trigger('chosen:updated');

				$.each(easyJson.states[i].outlets, function (index , value) {
					$('#rhb-outlet').append('<option value="'+value.easyName+'">'+value.easyName+'</option>').trigger('chosen:updated');
				});
			}
		}

	});


	$('#rhb-outlet').change(function(){

		if($(this).val() !== ""){
			$('#outlet-more-info').fadeOut('fast', function(){
				var selectedIndexState = $('#state')[0].selectedIndex-1;
				var selectedIndexRhb = $('#rhb-outlet')[0].selectedIndex-1;


				var address = easyJson.states[selectedIndexState].outlets[selectedIndexRhb];
				$('#outlet-address, #operation-hours').removeClass('animated fadeIn');

				$('#outlet-address').text(address.easyAddress);
				$('#operation-hours').text(address.easyOpHours);

				$('#outlet-more-info').fadeIn('fast');
			});
		}
		else {
			$('#outlet-more-info').fadeOut();
		}
	});

	$('#checkbox-clarify').on('change',function(){
		if($('#checkbox-clarify').is(':checked')){
			$('#rhb-btn-submit').prop('disabled',false);
		}
		else {
			$('#rhb-btn-submit').prop('disabled',true);
		}
	})

	function updateSlickHeight() {
		rhbform.slickSetOption('responsive', true, true);
	}

	//INIT chosen plugin
	$('.chosen-select').chosen({
		width: "100%",
		disable_search : true
	});

	var rhbform = $('#rhb-form'),
	progressform = $('.form-progress'),
	progressformChild = $('.form-progress .indicator');

	//INIT SLICK JS
	rhbform.slick({
		infinite: false,
		draggable: false,
		swipe: false,
		arrows: false,
		speed: 300,
		accessibility: false,
	  //adaptiveHeight: true,
	  slidesToShow: 1,
	  onAfterChange: function(slider, i) {
	  	progressformChild.removeClass('indicator__current indicator__done');
	  	progressformChild.eq(i).addClass('indicator__current');

	  	for(var num = i-1; num >= 0; num--) {
	  		progressformChild.eq(num).addClass('indicator__done');
	  	}
	  },
	  onInit : function(slider, i){
	  	progressformChild.eq(0).addClass('indicator__current');
	  }
	});

	// icon indicator
	$(progressform).slick({
		slide: '.indicator',
		asNavFor: '#rhb-form',
		slidesToShow: 2,
		slidesToScroll: 1,
		dots: true,
		arrows: false,
		draggable: false,
		swipe: false,
	});

	$('.back-link').on('click',function(e){
		rhbform.slickPrev();

		e.preventDefault();

	})

	$('#next-q').on('click',function(e){
		$('#rhb-financing-form').parsley().validate('blockdetails');
		//updateSlickHeight();


		if($('#rhb-financing-form').parsley().isValid('blockdetails')){
			//console.log('valid');
			$('html,body').animate({
				scrollTop : $('.form-heading').offset().top - 20
			})

			rhbform.delay(300).queue(function(next){
				rhbform.slickNext();

				next();
			});

			//$('#state').focus();
		}
		e.preventDefault();
	});

	$.listen('parsley:field:error', function(ParsleyField) {
		ParsleyField.$element.addClass('warning');
	});

	$.listen('parsley:field:success', function(ParsleyField) {
		ParsleyField.$element.removeClass('warning');
	});

});//#DOCUMENT READY