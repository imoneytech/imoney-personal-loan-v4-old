/** @jsx React.DOM */

var ProductCategoryRow = React.createClass({
    render: function() {
        return (<tr><th colSpan="2">{this.props.category}</th></tr>);
    }
});

var ProductRow = React.createClass({
    render: function() {
        var name = this.props.product.stocked ?
            this.props.product.name :
            <span style={{color: 'red'}}>
                {this.props.product.name}
            </span>;
        return (
            <tr>
                <td>{name}</td>
                <td>{this.props.product.price}</td>
            </tr>
        );
    }
});


var ProductTable = React.createClass({
    render: function() {
        var rows = [];
        var lastCategory = null;
        this.props.products.forEach(function(product) {
            if (product.name.indexOf(this.props.filterText) === -1 || (!product.stocked && this.props.inStockOnly)) {
                return;
            }
            if (product.category !== lastCategory) {
                rows.push(<ProductCategoryRow category={product.category} key={product.category} />);
            }
            rows.push(<ProductRow product={product} key={product.name} />);
            lastCategory = product.category;
        }.bind(this));
        return (
            <table>
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Price</th>
                </tr>
                </thead>
                <tbody>{rows}</tbody>
            </table>
        );
    }
});



    var SearchBar = React.createClass({
    //    handleChange: function() {
    //    this.props.onUserInput(
    //    this.refs.filterTextInput.getDOMNode().value,
    //    this.refs.inStockOnlyInput.getDOMNode().checked
    //    );
    //},
    render: function() {
    return (
        React.createElement('div', { className :"form-group" }, null ,
                React.createElement('label', null, "I want to borrow"),
                React.createElement('div', { className: "input-group input-group-lgsss" }, null,
                    React.createElement('input', {
                        id              : "input-amount" ,
                        type            : "text" ,
                        placeholder     : "3000" ,
                        className       :  "form-control input-lgsss"
                    }, null),
                React.createElement('div', { className :"form-group" }, null ,
                    React.createElement('label', null, "for"),
                    React.createElement('div', {className : "input-group input-group-lg"}, null,
                        React.createElement('select', { className:"form-control" }, null,
                        React.createElement('option', null, "1"),
                        React.createElement('option', null, "2"),
                        React.createElement('option', null, "3"),
                        React.createElement('option', null, "4"),
                        React.createElement('option', null, "5"),
                        React.createElement('option', null, "6")
                    ))))
        )
    )
}
});

  /* <div class="form-group">
    <label>I want to borrow</label>
    <div class="input-group input-group-lg"> <span class="input-group-addon">RM</span>
        <input id="input-amount" type="text" placeholder="30000" aria-describedby="input-amount" value="30000"
               class="form-control input-lg"></div>
    </div>
    <div class="form-group">
        <label>for</label>
        <div class="input-group input-group-lg">
            <select id="input-year" aria-describedby="input-year" class="form-control input-lg">
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3" selected="selected">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
                <option value="6">6</option>
                <option value="7">7</option>
                <option value="8">8</option>
                <option value="9">9</option>
                <option value="10">10</option>
            </select><span class="input-group-addon">year</span>
        </div>
    </div>
*/

//    var SearchBar = React.createClass({
//    handleChange: function() {
//        this.props.onUserInput(
//            this.refs.filterTextInput.getDOMNode().value,
//            this.refs.inStockOnlyInput.getDOMNode().checked
//        );
//    },
//    render: function() {
//        return (
//            <form>
//                <input
//                    type="text"
//                    placeholder="Search..."
//                    value={this.props.filterText}
//                    ref="filterTextInput"
//                    onChange={this.handleChange}
//                    />
//                <p>
//                    <input
//                        type="checkbox"
//                        checked={this.props.inStockOnly}
//                        ref="inStockOnlyInput"
//                        onChange={this.handleChange}
//                        />
//                    {' '}
//                    Only show products in stock
//                </p>
//            </form>
//        );
//    }
//});

var FilterableProductTable = React.createClass({
    getInitialState: function() {
        return {
            filterText: '',
            inStockOnly: false
        };
    },

    handleUserInput: function(filterText, inStockOnly) {
        this.setState({
            filterText: filterText,
            inStockOnly: inStockOnly
        });
    },

    render: function() {
        return (
            <div>
                <SearchBar
                    filterText={this.state.filterText}
                    inStockOnly={this.state.inStockOnly}
                    onUserInput={this.handleUserInput}
                    />
                <ProductTable
                    products={this.props.products}
                    filterText={this.state.filterText}
                    inStockOnly={this.state.inStockOnly}
                    />
            </div>
        );
    }
});


var PRODUCTS = [
    {category: 'Sporting Goods', price: '$49.99', stocked: true, name: 'Football'},
    {category: 'Sporting Goods', price: '$9.99', stocked: true, name: 'Baseball'},
    {category: 'Sporting Goods', price: '$29.99', stocked: false, name: 'Basketball'},
    {category: 'Electronics', price: '$99.99', stocked: true, name: 'iPod Touch'},
    {category: 'Electronics', price: '$399.99', stocked: false, name: 'iPhone 5'},
    {category: 'Electronics', price: '$199.99', stocked: true, name: 'Nexus 7'}
];

React.render(<SearchBar/>, document.getElementById("Emad"));
