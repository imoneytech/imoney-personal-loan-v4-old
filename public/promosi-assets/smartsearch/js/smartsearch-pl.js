function mergeFormData($form, extraData) {
  var paramObj = {};
  $.each($($form).serializeArray(), function (_, kv) {
    if (paramObj.hasOwnProperty(kv.name)) {
      paramObj[kv.name] = $.makeArray(paramObj[kv.name]);
      paramObj[kv.name].push(kv.value);
    }
    else {
      paramObj[kv.name] = kv.value;
    }
  });
  return $.extend({}, extraData, paramObj);
}

function getQueryVariable(variable) {
  var query = window.location.search.substring(1);
  var vars = query.split('&');
  for (var i = 0; i < vars.length; i++) {
    var pair = vars[i].split('=');
    if (decodeURIComponent(pair[0]) == variable) {
      return decodeURIComponent(pair[1]);
    }
  }
}

var utm_source = getQueryVariable('utm_source') ? getQueryVariable('utm_source') : '';
var utm_medium = getQueryVariable('utm_medium') ? getQueryVariable('utm_medium') : '';
var utm_term = getQueryVariable('utm_term') ? getQueryVariable('utm_term') : '';
var utm_content = getQueryVariable('utm_content') ? getQueryVariable('utm_content') : '';
var utm_campaign = getQueryVariable('utm_campaign') ? getQueryVariable('utm_campaign') : '';


//smart search title
var ssTitle = $('#ss-title');
$(document).ready(function () {
  if (!getQueryVariable('purpose')) {
    ssTitle.text('Berhemat dalam cicilan dengan KTA yang terbaik untuk Anda!');
  }
});


//slick
var slideQuestions = $('.smartsearch'),
  slideProgressBar = $('.ss-progressbar'),
  slideIndicator = $('.smartsearch-indicator ul'),
  slideButtons = $('.smartsearch-indicator span'),
  totalSlides;

slideQuestions.slick({
  infinite: false,
  draggable: false,
  swipe: false,
  arrows: false,
  speed: 300,
  adaptiveHeight: true,
  slidesToShow: 1,
  initialSlide: getQueryVariable('purpose') ? 1 : 0,
  onAfterChange: function (slider, i) {
    //remove all active class
    $(slideButtons).removeClass('icn_highlight');
    $(slideButtons).parent('li').removeClass('icn_highlight_done');
    $(slideButtons).css('opacity', '1');
    //set active class for current slide
    $(slideButtons).eq(i).addClass('icn_highlight');

    for (var num = i - 1; num >= 0; num--) {
      $(slideButtons).eq(num).addClass('icn_highlight').css('opacity', '0.2');
      $(slideButtons).eq(num).parent('li').addClass('icn_highlight_done');
    }

    //progress bar
    var currentSlide = slideQuestions.slickCurrentSlide() + 1;
    if ($(slideQuestions).length === 0) {
      slideProgressBar.width('100%');
    } else {
      slideProgressBar.width(currentSlide / totalSlides * 100 + '%');
    }
  },
  onInit: function (slider, i) {
    $('.promo').addClass('active').animate({
      'opacity': '1'
    })
    $('.promo-m').addClass('active').animate({
      'opacity': '1'
    })

    totalSlides = $('.slick-slide').length - $('.slick-cloned').length;
    slideProgressBar.width(1 / totalSlides * 100 + '%');
    //console.log(slideQuestions.slickCurrentSlide());
    // to re-select first buttons on first slide
    // $('.btns-spending li:first-child > label').click();
    if (getQueryVariable('purpose')) {
      // set active class to first slide to indicator
      $(slideButtons).eq(0).addClass('icn_highlight').css('opacity', '0.2');
      $(slideButtons).eq(0).parent('li').addClass('icn_highlight_done');
      $(slideButtons).eq(1).addClass('icn_highlight');
    } else {
      $(slideButtons).eq(0).addClass('icn_highlight');
      $('#purpose-dropdown').focus();
    }
  }
});

// icon indicator
$(slideIndicator).slick({
  slide: 'li',
  asNavFor: '.smartsearch',
  slidesToShow: 4,
  slidesToScroll: 1,
  dots: true
});

$('input[name="purpose"]').on('click', function () {
  if ($('input[name="purpose"]').is(':checked')) {
    $(slideQuestions).delay(500).queue(function (next) {
      $(slideQuestions).slickNext();
      next();
    });
  }
});


// go to next slide after select an answer
$('.btns-spending input, .salary input, .time input, .exist input, .balancetrnx input, #existingCC-no, .btn-next').on('click', function () {

  //scroll to top
  $('html, body').animate({
    scrollTop: $('.cc-smartsearch-header').offset().top + $('.cc-smartsearch-header').outerHeight()
  });

  $(slideQuestions).delay(500).queue(function (next) {

    $(slideQuestions).slickNext();

    next();
  });
});


function refreshSlickHeight() {
  $(slideQuestions).slickSetOption('responsive', true, true);
}

$('.btn-show').on('click', function (e) {

  $('#filter').parsley().validate();
  refreshSlickHeight();

  if ($('#filter').parsley().isValid()) {

    // if($('#filter').hasClass('submitted')) return true;

    $("#utsource").attr("value", getQueryVariable('utm_source'));
    $("#utmed").attr("value", getQueryVariable('utm_medium'));
    $("#utterm").attr("value", getQueryVariable('utm_term'));
    $("#utcont").attr("value", getQueryVariable('utm_content'));
    $("#utcam").attr("value", getQueryVariable('utm_campaign'));


    new CampaignFactory('id', undefined, function (factory) {
      var cam = factory.getByMachineName('ID_PL_iMoney_Smart_Search_KTA_2');

      cam.setFieldValue('name', $('#form-name').val());
      cam.setFieldValue('phone_number', $('#form-phone').val());
      cam.setFieldValue('email_address', $('#form-email').val());
      cam.setFieldValue('REASON', $('input[name*=spending]:checked').val());
      cam.setFieldValue('loan_amount_text', $('input[name*=loan-amount]:checked').val());
      cam.setFieldValue('salary_range', $('input[name*=salary]:checked').val());
      cam.setFieldValue('loan_tenure_text', $('input[name*=time]:checked').val());
      cam.setFieldValue('have_another_credit_card', $('input[name*=kredit]:checked').val());
      cam.setFieldValue('city', $('#form-city').val());
      cam.setFieldValue('gender', $('#form-gender').val());
      cam.setFieldValue('productname', $('#product').val());

      cam.submit(function (obj, status) {
        $('#filter button').html('Harap menunggu...').attr('disabled', 'disabled');
        if (status == 'complete') {
          $('#referrer').val(obj.data.$id);


          $('#filter').addClass('submitted');
          $('#filter').submit();
          var fData = mergeFormData($('#filter'), {});
          store.set('offersData', fData);
        }
      })
    });

    return false;
  }
});

// prefer-to-talk form validation
$('.btn-callback').on('click', function (e) {
  $('#prefer-to-talk-form').parsley().validate('block1');

  if ($('#prefer-to-talk-form').parsley().isValid('block1')) {
    submitPreferToTalk($('#prefer-to-talk-form'), function () {
      $('.PreferToTalk').modal('hide');
      $('#PreferToTalkThankYou').modal('show');

      //fix unassign modal-open class to body for second modal popup for mobile offset
      var body = $('body')
        .on('shown.bs.modal', '.modal', function () {
          body.addClass('modal-open')
        })
        .on('hidden.bs.modal', '.modal', function () {
          body.removeClass('modal-open')
        });
    });
  }

  return false;
});
