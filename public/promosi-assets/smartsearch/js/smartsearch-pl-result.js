$(document).ready(function () {

  function checkHeight(className) {
    var thisHeight, highest = 0;
    if ($(window).width() >= 992) {
      $(className).each(function () {

        thisHeight = $(this).innerHeight();

        if (thisHeight > highest) {
          highest = thisHeight;
        }
      });
      $(className).css('height', highest);
    }
  }

  checkHeight('.card-reward');

  /* Show the modal when apply button clicked */ 
  /* direct links for now */
  $('.button-wrapper button').on('click', function () {

    var btnData = $(this).data('value');
    console.log(btnData);

    //change button text to applied and make it disabled
    $(this).addClass('disabled').text('Selesai');


    new CampaignFactory('id', undefined, function (factory) {
      var cam = factory.getByMachineName('ID_PL_iMoney_Smart_Search_KTA_2');

      cam.setFieldValue('name', customer_name);
      cam.setFieldValue('phone_number', customer_phone);
      cam.setFieldValue('email_address', customer_email);
      cam.setFieldValue('loan_amount_text', customer_loanamount);
      cam.setFieldValue('salary_range', customer_salary);
      cam.setFieldValue('REASON', customer_spending);
      cam.setFieldValue('loan_tenure_text', customer_time);
      cam.setFieldValue('have_another_credit_card', customer_kredit);
      cam.setFieldValue('productname', btnData);
      cam.setFieldValue('city', customer_city);
      cam.setFieldValue('gender', customer_gender);


      cam.submit(function (obj, status) {
        // $('#filter button').html('Please wait...').attr('disabled', 'disabled');
        if (status == 'complete') {
          $('#referrer').val(obj.data.$id);
          // $('#filter').addClass('submitted');
          $('#filter').submit();
          window.location.href = btnData;
        }
      })
    });

  });

  /*** Call another modal when Done button is clicked ***/
  // $('')

  //submit form when select something in skip modal
  $('[name="result-skip-feedback"]').change(function () {
    $('.results-feedback').delay(700).queue(function (next) {
      $(this).submit();

      next();
    });
  });

  //add animate class to results wrapper depends on data-delay attribute
  $('.results-cards').each(function () {
    var dataDelay = $(this).data('delay') * 500;

    $(this).delay(dataDelay).queue(function (next) {

      $(this).children('.results-wrapper').addClass('animated flipInY');

      next();
    });
  });


  /**
   * Uses store.js to get the values from localStorage with a cookie storage fallback
   * For more info: https://github.com/marcuswestin/store.js/
   * @param name
   */
  function getParameterByName(name) {
    try {
      return store.get('offersData')[name];
    }
    catch(e) {
      console.error(e);
    }
  }

  function getQueryVariable(variable) {
    var query = window.location.search.substring(1);
    var vars = query.split('&');
    for (var i = 0; i < vars.length; i++) {
      var pair = vars[i].split('=');
      if (decodeURIComponent(pair[0]) == variable) {
        return decodeURIComponent(pair[1]);
      }
    }
    //console.log('Query variable %s not found', variable);
  }

  var customer_name = getParameterByName('name');
  var customer_email = getParameterByName('email');
  var customer_phone = getParameterByName('phone');
  var customer_spending = getParameterByName('spending');
  var customer_salary = getParameterByName('salary');
  var customer_loanamount = getParameterByName('loan-amount');
  var customer_time = getParameterByName('time');
  var customer_kredit = getParameterByName('kredit');
  var referrer_submission_id = getParameterByName('referrer_submission_id');
  var customer_gender = getParameterByName('gender');
  var customer_city = getParameterByName('city');

  $("#customer_name").text(customer_name);
  $("#customer_email").text(customer_email);
  $("#customer_phone").text(customer_phone);

  var utm_source = getQueryVariable('utm_source') ? getQueryVariable('utm_source') : '';
  var utm_medium = getQueryVariable('utm_medium') ? getQueryVariable('utm_medium') : '';
  var utm_term = getQueryVariable('utm_term') ? getQueryVariable('utm_term') : '';
  var utm_content = getQueryVariable('utm_content') ? getQueryVariable('utm_content') : '';
  var utm_campaign = getQueryVariable('utm_campaign') ? getQueryVariable('utm_campaign') : '';
});