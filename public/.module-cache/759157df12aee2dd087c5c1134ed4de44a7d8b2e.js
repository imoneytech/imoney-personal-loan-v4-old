// @codekit-prepend "rangeslider/rangeslider.js";
// @codekit-prepend "../../bower_components/imoney-assets/bower_components/bootstrap-sass/assets/javascripts/bootstrap/transition.js";
// @codekit-prepend "../../bower_components/imoney-assets/bower_components/bootstrap-sass/assets/javascripts/bootstrap/modal.js";
// @codekit-prepend "jquery.easing.min.js";
// @codekit-prepend "stickykit.min.js";

$(document).ready(function(){



	//---------------------------
	// RANGESLIDER
	//---------------------------
	var salary = $('.salary-howmuch');

	function updateOutput(el, val) {
	  el.text('RM' + val);
	}

	$('#salary').rangeslider({
		polyfill: false,
		onInit: function() {
			updateOutput(salary, this.value);
		}
	}).on('input', function(){
		updateOutput(salary, this.value);
	});


	//---------------------------
	// CALCULATOR
	//---------------------------

	// initiate value
	var mobileAmount = $('#mobile-amount'),
			mobileYear = $('#mobile-year'),
			inputAmount = $('#input-amount'),
			inputYear = $('#input-year'),
			calculatorMobile = $('.calculator--mobile');

	mobileAmount.text('RM' + inputAmount.val());
	mobileYear.text(inputYear.find(':selected').text() + ' years');

	calculatorMobile.on('click', function(e){
		if($(this).nextAll().is(':hidden')) {
			$(this).nextAll().slideDown();
			$(this).find('b').text('Hide Calculator');
		} else {
			$(this).nextAll().slideUp(function(){
				$('.calculator__main, .calculator__filters').removeAttr('style');
			});
			$(this).find('b').text('Show Calculator');
		}
		e.preventDefault();
		$('html, body').animate({scrollTop: $(this).offset().top}, 400);
	});

	$('.btn-calculator-close').on('click', function(e){

		$(this).addClass('btn-calculator-close--active').prop('disabled',true).delay(1000).queue(function(){
			calculatorMobile.find('b').text('Show Calculator');
			calculatorMobile.nextAll().slideUp(function(){
				$('.calculator__main, .calculator__filters').removeAttr('style');
			});
			$('html, body').animate({scrollTop: calculatorMobile.offset().top}, 400);
			$(this).prop('disabled',false).removeClass('btn-calculator-close--active');
		});
		e.preventDefault();
	});

	inputAmount.on('keyup', function(){
		mobileAmount.text('RM' + inputAmount.val());
	});

	inputYear.on('change', function(){
		var year = $(this).find(':selected').text();
		if (parseInt(year) === 1 ) {
			mobileYear.text(year + ' year');
		} else {
			mobileYear.text(year + ' years');
		}
	});




	//---------------------------
	// Show Hide more info
	//---------------------------

	var moreinfo = $('.btn-more-info'), time = 400;

	moreinfo.on('click', function(e){

		var tableColumns = $(this).parents('.table-columns');

		if($(this).hasClass('active')) {
			$(this).removeClass('active').find('span').text('more');
			tableColumns.removeClass('table-columns--expand').siblings('.col-more-info').slideUp(time);
			$('html, body').animate({scrollTop: $(this).parents('.table__product').offset().top}, time);
		} else {
			$(this).addClass('active').find('span').text('less');
			tableColumns.addClass('table-columns--expand').siblings('.col-more-info').slideDown(time);
			$('html, body').animate({scrollTop: $(this).parents('.table__product').offset().top}, time);
		}
		e.preventDefault();
	});

	$('.btn-close').on('click', function(e){
		var sibling = $(this).parents('.col-more-info').siblings('.table-columns');
		sibling.removeClass('table-columns--expand').find('.btn-more-info').removeClass('active').find('span').text('more');
		$('html, body').animate({scrollTop: $(this).parents('.table__product').offset().top}, time);
		$(this).parents('.col-more-info').slideUp(time);
		e.preventDefault();
	});




	//---------------------------
	// Compare product
	//---------------------------

	// add bank to the Compare panel at sidebar
	function addBank(bankId, bankName) {
		$('<div class="cp-product" id="' + bankId + '"><div class="cp-product-name">' + bankName + '</div><div class="cp-product-close"><a class="close" href="#"></a></div>').appendTo('#cp-product-list').hide().fadeIn();
		$(document.body).trigger('sticky_kit:recalc');
	}

	// remove bank to the Compare panel at sidebar
	function removeBank(bankId) {
		$('#' + bankId).remove();
		$(document.body).trigger('sticky_kit:recalc');
	}

	// calculate number of checked product
	function hasProduct(){

		var num = $('#cp-product-list').children().length;
		var btn = $('.btn-cp-product');

		btn.text('Compare (' + num + '/3)');

		if(num) {
			$('.cp-product--empty').hide();

			if (num > 1) {
				btn.prop('disabled', false).removeClass('btn-cp-product--disabled');
			} else if (num < 2) {
				btn.prop('disabled', true).addClass('btn-cp-product--disabled');
			}
		} else {
			$('.cp-product--empty').show();
		}
	}

	$('.col-compare-btn').on('click', function(){
		var checkedBank = $(this).parents('.table__product').data('bank');
		var checkedBankId = $(this).parents('.table__product').data('id');

		//if is not checked, check it
		if($(this).hasClass('col-compare-btn--active')) {
			removeBank(checkedBankId);
			$(this).removeClass('col-compare-btn--active');
		} else {

			if ($('#cp-product-list').children().length === 3) {
				alert('Max number reached. Remove one product to add this');
			} else {
				$(this).addClass('col-compare-btn--active');
				addBank(checkedBankId, checkedBank);
			}
		}
		hasProduct();
	});

	$('.cp-product-list').on('click', '.cp-product a', function(e){
			var bankId =  $(this).closest('.cp-product').prop('id');
			removeBank(bankId);
			hasProduct();
			$('.table__product[data-id="' + bankId + '"]').find('.col-compare-btn').removeClass('col-compare-btn--active');
			e.preventDefault();
	});




	//---------------------------
	// MODAL & FORM
	//---------------------------

	$('.helper--faq').on('click', function(e){
		$('html, body').animate({scrollTop: $('#faq').offset().top}, 1000, 'easeInOutQuad');
		e.preventDefault();
	});

	// Modal Compare
	$('#btn-cp-product').on('click',function(e){
		$('#modal-compare').modal({
			show: true
		});
		e.preventDefault();
	});

	// Modal apply
	$('.btn-apply').on('click', function(e){
		$('#modal-apply').modal({
			show: true
		});
		e.preventDefault();
	});


	// Modal How
	$('.helper--how').on('click', function(e){
		$('#modal-how').modal({
			show: true
		});
		e.preventDefault();
	});

	// Modal Enquire
	$('.helper--ask').on('click', function(e){
		$('#modal-enquire').modal({
			show: true
		});
		e.preventDefault();
	});

	$('#modal-enquire').on('hidden.bs.modal', function(){
		$(this).find('.modal-form').show().next().hide().removeClass('is-visible');
		$('.enquire-bee').removeClass('enquire-bee--success');
	});

	// enquiry submit on success
	$('#form-enquire').on('submit', function(e){
		$(this).closest('.modal-form').hide().next().fadeIn();
		$('.icon-success').addClass('icon-success-in');
		$('.modal-success').addClass('is-visible');
		$('.enquire-bee').addClass('enquire-bee--success');
		e.preventDefault();
	});


	// StickyKit
	$('.section-faq .helper').stick_in_parent({
		offset_top: 30
	});

	$('.compare-products').stick_in_parent({
		parent: '.comparison-side',
		offset_top: 10
	});

});
