// Create a new module
var app = angular.module('PersonalLoanApp', ['react']);
app.controller('PersonalLoanController', function($scope, $http, personalLoanInterestManager) {
    angular.element(document).ready(function() {
        $scope.data;
        $scope.tmp = [];
        if(checkbusiness && region==='my'){
            $scope.filterAmount = 100000;
        } else {
            $scope.filterAmount = loan_amount;
        }
        $scope.filterMonthlyIncome = DefaultMonthly_Salary;
        $scope.filterTenure = tenure;
        $scope.government = false;
        $scope.islamic = false;
        $scope.salary = 3000;
        var logic = false;
        // expect($('#result-number').getAttribute('ng-cloak')).toBeNull();
        //$scope.filterMonthlyIncome = $("input[type=range]").val();

        $scope.init = function() {
            $http({
                method: 'GET',
                url: fetchdata
            }).then(function successCallback(response) {
                if(checkbusiness && region==='my'){
                    $scope.data = _.filter(response.data, function(loan){ return loan.smallBusinessLoans == "1"; });
                }
                else{
                    $scope.data = response.data;
                }
                $scope.update();
                logic = true;
            }, function errorCallback(response) {
                $scope.data = [];
            });
        }

        $scope.setSalary = function(salary) {
            $scope.filterMonthlyIncome = salary;
            if (logic)
                $scope.update();
        }

        $scope.getInterestRateFromKeyRange = function(keys, keyname, personalLoanRates, print) {
            //Sort asc by keyname
            personalLoanRates.sort(function(a, b) {
                return parseInt(a[keyname]) - parseInt(b[keyname]);
            });

            var item = $scope.getItemFromRange(keys[0], keyname, personalLoanRates);

            if (!item) return null;

            return item['value'];
        }

        $scope.getInterestRateFromAmountTenure = function(keys, personalLoanRates) {
                var amount = keys[0];
                var tenure = keys[1];
                var rates = [];

                //Find the rates by tenure
                angular.forEach(personalLoanRates, function(rate, key) {
                    if (parseInt(rate.key2) == parseInt(tenure)) {
                        rates.push({
                            key: rate.key1,
                            value: rate.value
                        });
                    }
                });

                if (rates.length == 0) return null;

                //Sort rates by income asc
                rates.sort(function(a, b) {
                    return parseInt(a.key) - parseInt(b.key);
                });

                //Now find rate from amount range
                var item = this.getItemFromRange(amount, 'key', rates);
                if (!item) return null;

                return item['value'];
            },

            $scope.getInterestRateFromTenure = function(keys, personalLoanRates) {
                var tenure = keys[0];
                var irate = null;

                angular.forEach(personalLoanRates, function(rate, key) {
                    if (parseInt(rate.key1) == parseInt(tenure)) {
                        irate = rate.value;
                        return;
                    }
                });

                return irate;
            }

        $scope.getInterestRateFromKeyRangeKeyRange = function(keys, personalLoanRates) {
            var key1 = keys[0];
            var key2 = keys[1];
            var rates = [];

            //Filtering by key1 first
            rates = this.filterRatesByKeyRange(key1, 'key1', personalLoanRates);
            if (!rates.length) return null;

            //Filtering by key2
            rates = this.filterRatesByKeyRange(key2, 'key2', rates);
            if (!rates.length) return null;

            return rates[0].value;
        }

        $scope.filterCondition = function(personalLoan, Enablesticky) {
            var Ok = true;
            if ($scope.government) {
                if (personalLoan.governmentOnly != 'Yes') {
                    Ok = false;
                }
            } else if (personalLoan.governmentOnly == 'Yes') Ok = false;
            if ($scope.islamic) {
                if (personalLoan.islamic_conventional == "Conventional") {
                    Ok = false;
                }
                if ($scope.government) {
                    if (personalLoan.governmentOnly != 'Yes') {
                        Ok = false;
                    }
                } else if (personalLoan.governmentOnly == 'Yes') Ok = false;
            }
            if ((parseInt($scope.filterAmount) < parseInt(personalLoan.minimumAmount))) {
                Ok = false;
            }
            if (parseInt($scope.filterAmount) > parseInt(personalLoan.maximumAmount)) {
                Ok = false;
            }

            if (parseInt($scope.filterMonthlyIncome) < parseInt(personalLoan.minimumIncome)) {
                Ok = false;
            }
            if (parseInt(personalLoan.maximumTenure) < parseInt($scope.filterTenure)) {
                Ok = false;
            }
            if ((parseInt(personalLoan.minimumTenure) > parseInt($scope.filterTenure))) {
                Ok = false;
            }
            if (Enablesticky)
                if (personalLoan.field_sticky_feature == "1") {
                    Ok = true;
                }
            return Ok;
        }

        $scope.filterConditionsmallbusniss = function(personalLoan) {
            var Ok = true;
            // ID logic
            if (region == "id") {
                if ((personalLoan.personalLoanType).indexOf("With Collateral") > -1) {
                    Ok = false;
                }
            }

            // PH logic
            if (personalLoan.smallBusinessLoans == "0") {
                Ok = false;
            }
            if ((parseInt($scope.filterAmount) < parseInt(personalLoan.minimumAmount))) {
                Ok = false;
            }
            if (parseInt($scope.filterAmount) > parseInt(personalLoan.maximumAmount)) {
                Ok = false;
            }
            if (parseInt($scope.filterMonthlyIncome) < parseInt(personalLoan.minimumIncome)) {
                Ok = false;
            }
            if (parseFloat(personalLoan.maximumTenure) < parseFloat($scope.filterTenure)) {
                Ok = false;
            }
            if ((parseFloat(personalLoan.minimumTenure) > parseFloat($scope.filterTenure))) {
                Ok = false;
            }
            if (personalLoan.field_sticky_feature == "1") {
                Ok = true;
            }
            return Ok;
        }



        $scope.update = function() {
            if ($scope.filterAmount == null) {
                var tmp = true;
                $scope.tmp.length = 0;
            }
            $scope.tmp.length = 0;
            for (i = 0; i < $scope.data.length; i++) {
                var checkbank = $scope.filterCondition($scope.data[i], true);
                if (checkbusiness)
                    checkbank = $scope.filterConditionsmallbusniss($scope.data[i]);
                if (checkislamic) {
                    $scope.islamic = true;
                    checkbank = $scope.filterCondition($scope.data[i], false);
                }
                if (checkbank) {
                    $scope.tmp.push(personalLoanInterestManager.calculation($scope.data[i], $scope.filterAmount, $scope.filterMonthlyIncome, $scope.filterTenure, tmp));
                }
                $scope.tmp.sort(function(obj1, obj2) {
                    return obj1.scoreorder - obj2.scoreorder;
                });
            }
            $scope.tmp.sort(function(obj1, obj2) {
                return obj1.scoreorder - obj2.scoreorder;
            });

            $.event.trigger({
                type: "personalLoadDataReady",
                message: $scope.tmp
            });
        };

        $scope.init();
    });
});

if (typeof(Personalloan) != "undefined") {
    app.value('Personalloan', Personalloan);
    app.directive('pl', function(reactDirective) {
        return reactDirective(Personalloan, ['params'], {
            restrict: 'A'
        });
    });
}


app.directive("slider", function() {
    return {
        restrict: 'A',
        link: function(scope, elem, attrs) {}
    }
});
$(document).ready(function() {
    dataLayer.push({
        'event': 'pl_angular_loaded'
    });
});
app.factory('personalLoanInterestManager', function() {
    return {
        getInterestRate: function(interestFormat, keys, personalLoanRates) {
            if (!interestFormat) return null;
            if (!keys) return null;
            if (!personalLoanRates) return null;

            if (interestFormat == 'tenurerange') return this.getInterestRateFromKeyRange(keys, 'key1', personalLoanRates);
            if (interestFormat == 'income') return this.getInterestRateFromKeyRange(keys, 'key1', personalLoanRates, 1);
            if (interestFormat == 'amount') return this.getInterestRateFromKeyRange(keys, 'key1', personalLoanRates);
            if (interestFormat == 'tenure') return this.getInterestRateFromTenure(keys, personalLoanRates); //No range. Absolute match only.
            if (interestFormat == 'tenure_income') return this.getInterestRateFromTenureIncome(keys, personalLoanRates);
            if (interestFormat == 'tenure_amount') return this.getInterestRateFromKeyRangeKeyRange(keys, personalLoanRates);
            if (interestFormat == 'amount_tenure') return this.getInterestRateFromAmountTenure(keys, personalLoanRates);
        },
        getInterestRateFromTenure: function(keys, personalLoanRates) {
            var tenure = keys[0];
            var irate = null;

            angular.forEach(personalLoanRates, function(rate, key) {
                if (parseFloat(rate.key1) == parseFloat(tenure)) {
                    irate = rate.value;
                    return;
                }
            });

            return irate;
        },
        filterRatesByKeyRange: function(niddle, niddleKey, rates) {
            var filteredRates = [];
            var highestFloor = null;

            //Sort asc by keyname
            rates.sort(function(a, b) {
                return parseFloat(a[niddleKey]) - parseFloat(b[niddleKey]);
            });

            //Find rates that are bigger
            angular.forEach(rates, function(rate, key) {
                if (parseFloat(niddle) >= parseFloat(rate[niddleKey])) {
                    filteredRates.push(rate);
                    highestFloor = rate[niddleKey];
                }
            });

            //Only keep the highest floor
            var ret = [];
            angular.forEach(filteredRates, function(rate, key) {
                if (parseFloat(highestFloor) == parseFloat(rate[niddleKey])) {
                    ret.push(rate);
                }
            });

            return ret;
        },
        getInterestRateFromKeyRangeKeyRange: function(keys, personalLoanRates) {
            var key1 = keys[0];
            var key2 = keys[1];
            var rates = [];

            //Filtering by key1 first
            rates = this.filterRatesByKeyRange(key1, 'key1', personalLoanRates);
            if (!rates.length) return null;

            //Filtering by key2
            rates = this.filterRatesByKeyRange(key2, 'key2', rates);
            if (!rates.length) return null;

            return rates[0].value;
        },
        getInterestRateFromKeyRange: function(keys, keyname, personalLoanRates, print) {
            //Sort asc by keyname
            personalLoanRates.sort(function(a, b) {
                return parseFloat(a[keyname]) - parseFloat(b[keyname]);
            });

            var item = this.getItemFromRange(keys[0], keyname, personalLoanRates);

            if (!item) return null;
            return item['value'];
        },
        getInterestRateFromTenureIncome: function(keys, personalLoanRates) {
            var tenure = keys[0];
            var income = keys[1];
            var rates = [];

            //Find the rates by tenure
            angular.forEach(personalLoanRates, function(rate, key) {
                if (parseFloat(rate.key1) == parseFloat(tenure)) {
                    rates.push({
                        key: rate.key2,
                        value: rate.value
                    });
                }
            });

            if (rates.length == 0) return null;

            //Sort rates by income asc
            rates.sort(function(a, b) {
                return parseFloat(a.key) - parseFloat(b.key);
            });

            //Now find rate from income range
            var item = this.getItemFromRange(income, 'key', rates);
            if (!item) return null;
            return item['value'];
        },
        getInterestRateFromAmountTenure: function(keys, personalLoanRates) {
            var amount = keys[0];
            var tenure = keys[1];
            var rates = [];

            //Find the rates by tenure
            angular.forEach(personalLoanRates, function(rate, key) {
                if (parseFloat(rate.key2) == parseFloat(tenure)) {
                    rates.push({
                        key: rate.key1,
                        value: rate.value
                    });
                }
            });

            if (rates.length == 0) return null;

            //Sort rates by income asc
            rates.sort(function(a, b) {
                return parseFloat(a.key) - parseFloat(b.key);
            });

            //Now find rate from amount range
            var item = this.getItemFromRange(amount, 'key', rates);
            if (!item) return null;

            return item['value'];
        },
        getItemFromRange: function(niddle, niddleKey, rates) {
            var item = null;

            angular.forEach(rates, function(rate, key) {
                if (parseFloat(niddle) >= parseFloat(rate[niddleKey])) {
                    item = rate;
                }
            });

            return item;
        },
        calculation: function(data, filterAmount, filterMonthlyIncome, filterTenure, tmp) {
            var searchKeys = this.getInterestRateSearchKeys(data.interestFormat, filterAmount, filterMonthlyIncome, filterTenure);
            data['Currency'] = Currency;
            data['interestRate'] = this.getInterestRate(data.interestFormat, searchKeys, data.personalLoanRates);
            var interestRate = data['interestRate'];
            data['minIRate'] = Math.min.apply(null, this.returnRate(data.personalLoanRates));
            //
            if (number_spreater) {
                data['minimumAmount_spreater'] = data.minimumAmount.replace(/\B(?=(\d{3})+(?!\d))/g, ".");
                data['maximumAmount_spreater'] = data.maximumAmount.replace(/\B(?=(\d{3})+(?!\d))/g, ".");
                data['minimumIncome_spreater'] = data.minimumIncome.replace(/\B(?=(\d{3})+(?!\d))/g, ".");
            } else {
                data['minimumAmount_spreater'] = data.minimumAmount.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                data['maximumAmount_spreater'] = data.maximumAmount.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                data['minimumIncome_spreater'] = data.minimumIncome.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            }
            if (data.field_loan_score == null) {
                data['scoreorder'] = parseInt(interestRate);
            } else {
                data['scoreorder'] = parseInt(interestRate ? interestRate : 0) - parseInt(data.field_loan_score);
            }

            data['maxIRate'] = Math.max.apply(null, this.returnRate(data.personalLoanRates));
            if (parseInt(data.maximumAmount) < parseInt(filterAmount) || parseInt(data.minimumAmount) > parseInt(filterAmount) || tmp ||
                parseInt(data.minimumIncome) > parseInt(filterMonthlyIncome) || interestRate == null) {
                data['interestRate'] = "-";
                data['installment'] = "-";
                data['months'] = "-";
                data['TotalAmountPayable'] = "-";
                data['TotalInterestPayable'] = "-";
            } else {
                if (number_spreater) {
                    data['filterAmount'] = filterAmount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
                } else {
                    data['filterAmount'] = filterAmount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                }
                if (parseInt(filterTenure) == 1) {
                    data['filterTenure'] = filterTenure + ' ' + translate("year");
                } else data['filterTenure'] = filterTenure + ' ' + translate(" years");

                data['interestRate'] = parseFloat(data['interestRate']).toFixed(2);
                if (number_spreater) {
                    data['interestRate'] = data['interestRate'].replace(".", ",");
                }

                var totalIRate = this.calculateTotalInterest(filterAmount, filterTenure, data.interestRate);
                var yearlyInterest = (filterAmount * interestRate) / 100;
                var monthlyInterest = yearlyInterest / 12;
                var principalInstallment = filterAmount / (parseFloat(filterTenure) * 12);
                var installment = monthlyInterest + principalInstallment;
                if (number_spreater) {
                    data['installment'] = parseInt(monthlyInterest + principalInstallment).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
                } else {
                    data['installment'] = parseInt(monthlyInterest + principalInstallment).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                }

                data['months'] = filterTenure * 12;

                if (number_spreater) {
                    data['TotalAmountPayable'] = (Math.round((parseFloat(installment) * filterTenure * 12) * 100) / 100).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
                    var TotalAmountPayable = (parseFloat(installment) * filterTenure) * 12;
                    data['TotalInterestPayable'] = (TotalAmountPayable - filterAmount).toFixed(0).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
                } else {
                    data['TotalAmountPayable'] = (Math.round((parseFloat(installment) * filterTenure * 12) * 100) / 100).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                    var TotalAmountPayable = (parseFloat(installment) * filterTenure) * 12;
                    data['TotalInterestPayable'] = (TotalAmountPayable - filterAmount).toFixed(0).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

                }

                if (data.field_loan_score == null) {
                    data['scoreorder'] = parseInt(interestRate);
                } else {
                    data['scoreorder'] = parseInt(interestRate) - parseInt(data.field_loan_score);
                }
            }

            if (data['field_feature_this_loan'] == "0") {
                data['track_region'] = "Listing Content Main";
                data['className'] = 'table__product';
                data['col'] = '';

            } else {
                data['track_region'] = "Listing Content Main Featured";
                data['className'] = 'table__product table__product--promo';
                data['col'] = 'col-promo';
            }
            // translation words pass it react component

            // data['Interest_rate'] = translate('Interest rate');
            data['Monthly_repayment'] = translate('Monthly repayment');
            data['Summary'] = translate('Summary');
            data['Total_interest_payable'] = translate('Total interest payable');
            data['Total_amount_payable'] = translate('Total amount payable');
            data['Product_features'] = translate('Product features');
            data['Eligibility'] = translate('Eligibility');
            data['Financing_amount_min_to_max'] = translate('Financing amount (min to max)');
            data['Min_salary_requirement'] = translate('Min. salary requirement');
            data['Employment_eligibility'] = translate('Employment eligibility');
            if (data.islamic_conventional === "Islamic") {
                data['Interest_rate'] = translate('Profit rate');
                data['Total_interest_payable'] = translate('Total profit payable');
                data['with_interest_rate_of'] = translate('with profit rate of');
                data['Loan_tenure_min_to_max'] = translate('Financing tenure (min to max)');
            } else {
                data['Interest_rate'] = translate('Interest rate');
                data['Total_interest_payable'] = translate('Total interest payable');
                data['with_interest_rate_of'] = translate('with interest rate of');
                data['Loan_tenure_min_to_max'] = translate('Loan tenure (min to max)');
            }
            // Loan tenure (min to max) is missing

            data['Age_requirement_min_max'] = translate('Age requirement (min-max)');
            data['Approval_duration'] = translate('Approval duration');
            data['Monthly_repayment'] = translate('Monthly repayment');
            data['Close_panel'] = translate('Close panel');
            data['Apply_Now'] = translate('Apply Now');
            data['More'] = translate('More');
            data['info'] = translate('info');
            data['less'] = translate('less');

            data['gross_per_month'] = translate('gross per month');

            data['years'] = translate('years');

            data['years_old'] = translate('years old');

            data['Fixed_rate_of'] = translate('Fixed rate of');
            data['per_annum'] = translate('per annum');
            data['for'] = translate('for');
            data['translate_months'] = translate('months');

            return data;
        },
        calculateTotalInterest: function(loan_amount, tenure, interest_rate) {
            var final_interest_rate = interest_rate / 100;
            var totalInterest = final_interest_rate * loan_amount * tenure / 12
            return totalInterest;
        },
        getInterestRateSearchKeys: function(interestFormat, filterAmount, filterMonthlyIncome, filterTenure) {
            var interestFormatString = typeof interestFormat == 'object' ? interestFormat[0] : interestFormat;
            switch (interestFormatString) {
                case 'tenure':
                case 'tenurerange':
                    return [filterTenure];
                case 'amount':
                    return [filterAmount];
                case 'income':
                    return [filterMonthlyIncome];
                case 'tenure_income':
                    return [filterTenure, filterMonthlyIncome];
                case 'tenure_amount':
                    return [filterTenure, filterAmount];
                case 'amount_tenure':
                    return [filterAmount, filterTenure];
            }
        },
        returnRate: function(personalLoanRates) {
            var Ratesarray = [];
            for (var i = 0; i < personalLoanRates.length; i++) {
                Ratesarray.push(personalLoanRates[i].value)
            }
            return Ratesarray;
        }
    };
});