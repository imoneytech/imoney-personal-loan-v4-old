"use strict";

function numProps(obj) {
    var c = 0;
    for (var key in obj) {
        if (obj.hasOwnProperty(key)) ++c;
    }
    return c;
}

var Plcompo = React.createClass({
    displayName: "Plcompo",
    render: function () {
        return (
            //edit span here
            React.createElement(
                "div",
                { "data-href": this.props.data.field_apply_url, "data-cross-sell": this.props.data.field_pl_cross_sell_product, "data-bank": this.props.data.bank, "data-title": this.props.data.title, "data-url-product-page": this.props.data.product_path, "data-img": this.props.data.bankLogo, "data-minage": this.props.data.minimumAge,
                    "data-maxage": this.props.data.maximumAge, "data-product-path": this.props.data.product_path, "data-nid": this.props.data.nid, "data-id": this.props.data.nid, "data-interestrate": this.props.data.interestRate,
                    "data-installment": this.props.data.installment, "data-eligibility": this.props.data.field_employment_eligibility_tex,
                    "data-approvalduration": this.props.data.field_approval_duration, "data-minimumtenure": this.props.data.minimumTenure,
                    "data-maximumtenure": this.props.data.maximumTenure, "data-minimumincome": this.props.data.minimumIncome_spreater,
                    "data-highlight": this.props.data.field_product_highlight, "data-applyformtype": this.props.data.formtype,
                    "data-minimumamount": this.props.data.minimumAmount, "data-maximumamount": this.props.data.maximumAmount,
                    "data-minimumAmount_spreater": this.props.data.minimumAmount_spreater, "data-jappmachinename": this.props.data.jappMachineName, "data-maximumAmount_spreater": this.props.data.maximumAmount_spreater,
                    "data-totalamountpayable": this.props.data.TotalAmountPayable, "data-totalinterestpayable": this.props.data.TotalInterestPayable,
                    className: this.props.data.className },
                React.createElement(
                    "div",
                    { className: this.props.data.col },
                    React.createElement("i", null),
                    this.props.data.field_promotional_text
                ),
                React.createElement(
                    "div",
                    { className: "table-columns" },
                    React.createElement(
                        "div",
                        { className: "col-compare col-sm-5p hidden-xs" },
                        React.createElement("div", { className: "col-compare-btn" })
                    ),
                    React.createElement(
                        "div",
                        { className: "col-product" },
                        React.createElement("img", { src: this.props.data.bankLogo, className: "col-product-image" }),
                        React.createElement(
                            "div",
                            { className: "col-product-info" },
                            React.createElement(
                                "h3",
                                null,
                                React.createElement(
                                    "div",
                                    { "data-imu-track": "a" },
                                    React.createElement(
                                        "a",
                                        { href: ["2965", "2962", "2961"].indexOf(this.props.data.nid) >= 0 ? "http://offers.imoney.my/campaign/personal-loan/affinbank-sme-financing" : "/" + this.props.data.product_path, "data-subject": "User",
                                            "data-action": "Click", "data-description": "Product Title", "data-region": "Listing Content Main", "data-vertical": "Personal Loan" },
                                        this.props.data.title
                                    )
                                )
                            ),
                            React.createElement(
                                "p",
                                null,
                                this.props.data.field_product_highlight
                            ),
                            React.createElement(
                                "a",
                                { href: "", className: "btn-more-info" },
                                React.createElement("i", null),
                                React.createElement(
                                    "span",
                                    null,
                                    this.props.data.More
                                ),
                                " ",
                                this.props.data.info
                            )
                        )
                    ),
                    React.createElement(
                        "div",
                        { className: "col-rate col-sm-18p col-xxs-50p" },
                        React.createElement(
                            "span",
                            { className: "col-rate--title col-title--small" },
                            this.props.data.Interest_rate
                        ),
                        React.createElement(
                            "div",
                            { className: "col-rate--item font-xlg" },
                            this.props.data.interestRate,
                            "%"
                        )
                    ),
                    React.createElement(
                        "div",
                        { className: "col-rate col-sm-18p col-xxs-50p" },
                        React.createElement(
                            "span",
                            { className: "col-rate--title col-title--small" },
                            this.props.data.Monthly_repayment
                        ),
                        React.createElement(
                            "div",
                            { className: "col-rate--item font-xlg" },
                            this.props.data.Currency,
                            " ",
                            this.props.data.installment
                        ),
                        React.createElement(
                            "span",
                            { className: "product-repayment-period extra-info" },
                            this.props.data.months,
                            " ",
                            this.props.data.translate_months
                        )
                    ),
                    React.createElement(
                        "div",
                        { className: "col-apply col-sm-18p", "data-imu-track": "a" },
                        React.createElement(
                            "a",
                            { "data-href": this.props.data.field_apply_url, "data-url-product-page": this.props.data.product_path, href: this.props.data.field_apply_url, rel: "nofollow", "data-img": this.props.data.bankLogo, "data-minage": this.props.data.minimumAge,
                                "data-maxage": this.props.data.maximumAge, "data-id": this.props.data.nid, "data-nid": this.props.data.nid, "data-interestrate": this.props.data.interestRate,
                                "data-installment": this.props.data.installment, "data-eligibility": this.props.data.field_employment_eligibility_tex,
                                "data-approvalduration": this.props.data.field_approval_duration, "data-minimumtenure": this.props.data.minimumTenure, "data-maximumtenure": this.props.data.maximumTenure, "data-minimumincome": this.props.data.minimumIncome,
                                "data-minimumamount": this.props.data.minimumAmount, "data-maximumamount": this.props.data.maximumAmount,
                                "data-highlight": this.props.data.field_product_highlight, "data-jappmachinename": this.props.data.jappMachineName,
                                "data-applyformtype": this.props.data.formtype, className: "btn btn-success btn-apply", "data-subject": "User", "data-action": "Click",
                                "data-title": this.props.data.title, "data-value": this.props.data.title, "data-description": "Apply Button", "data-region": this.props.data.track_region, "data-vertical": "Personal Loan",
                                "data-bank": this.props.data.bank, "data-product-path": this.props.data.product_path, "data-minirate": this.props.data.minIRate, "data-islamic_conventional": this.props.data.islamic_conventional,
                                "data-maxirate": this.props.data.maxIRate, "data-cross-sell": this.props.data.field_pl_cross_sell_product, className: this.props.data.quickApplyButton == "1" ? "btn btn-success btn-apply btn-qab" : "btn btn-success btn-apply" },
                            this.props.data.applyButtonText || this.props.data.Apply_Now
                        )
                    )
                ),
                React.createElement(
                    "div",
                    { className: "col-more-info" },
                    React.createElement(
                        "div",
                        { className: "info-summary" },
                        React.createElement(
                            "ul",
                            null,
                            React.createElement(
                                "li",
                                { className: "col-xxs-100 col-xs-50p" },
                                React.createElement(
                                    "strong",
                                    null,
                                    this.props.data.Summary
                                ),
                                this.props.data.Currency,
                                " ",
                                this.props.data.filterAmount,
                                " ",
                                this.props.data["for"],
                                " ",
                                this.props.data.filterTenure,
                                " ",
                                this.props.data.with_interest_rate_of,
                                " ",
                                this.props.data.interestRate,
                                "% ",
                                this.props.data.per_annum
                            ),
                            React.createElement(
                                "li",
                                { className: "col-xxs-33p col-xs-14p" },
                                React.createElement(
                                    "strong",
                                    null,
                                    this.props.data.Monthly_repayment
                                ),
                                this.props.data.Currency,
                                " ",
                                this.props.data.installment
                            ),
                            React.createElement(
                                "li",
                                { className: "col-xxs-33p col-xs-18p" },
                                React.createElement(
                                    "strong",
                                    null,
                                    this.props.data.Total_interest_payable
                                ),
                                this.props.data.Currency,
                                " ",
                                this.props.data.TotalInterestPayable
                            ),
                            React.createElement(
                                "li",
                                { className: "col-xxs-33p col-xs-18p" },
                                React.createElement(
                                    "strong",
                                    null,
                                    this.props.data.Total_amount_payable
                                ),
                                this.props.data.Currency,
                                " ",
                                this.props.data.TotalAmountPayable
                            )
                        )
                    ),
                    React.createElement(
                        "div",
                        { className: "info-features" },
                        React.createElement(
                            "div",
                            { className: "features-key col-xs-50p" },
                            React.createElement(
                                "h5",
                                null,
                                React.createElement("i", { className: "sprites sprites-star-blue" }),
                                this.props.data.Product_features
                            ),
                            React.createElement(
                                "ul",
                                null,
                                React.createElement(
                                    "li",
                                    null,
                                    React.createElement(
                                        "strong",
                                        null,
                                        this.props.data.Financing_amount_min_to_max
                                    ),
                                    this.props.data.Currency,
                                    " ",
                                    this.props.data.minimumAmount_spreater,
                                    " - ",
                                    this.props.data.Currency,
                                    " ",
                                    this.props.data.maximumAmount_spreater
                                ),
                                React.createElement(
                                    "li",
                                    null,
                                    React.createElement(
                                        "strong",
                                        null,
                                        this.props.data.Loan_tenure_min_to_max
                                    ),
                                    this.props.data.minimumTenure,
                                    " - ",
                                    this.props.data.maximumTenure,
                                    " ",
                                    this.props.data.years
                                ),
                                React.createElement(
                                    "li",
                                    null,
                                    React.createElement(
                                        "strong",
                                        null,
                                        this.props.data.Interest_rate
                                    ),
                                    this.props.data.Fixed_rate_of,
                                    " ",
                                    this.props.data.minIRate,
                                    "% - ",
                                    this.props.data.maxIRate,
                                    "% ",
                                    this.props.data.per_annum
                                ),
                                React.createElement(
                                    "li",
                                    null,
                                    React.createElement(
                                        "strong",
                                        null,
                                        this.props.data.Approval_duration
                                    ),
                                    this.props.data.field_approval_duration
                                )
                            )
                        ),
                        React.createElement(
                            "div",
                            { className: "features-eligibility col-xs-50p" },
                            React.createElement(
                                "h5",
                                null,
                                React.createElement("i", { className: "sprites sprites-tick-blue" }),
                                this.props.data.Eligibility
                            ),
                            React.createElement(
                                "ul",
                                null,
                                React.createElement(
                                    "li",
                                    null,
                                    React.createElement(
                                        "strong",
                                        null,
                                        this.props.data.Min_salary_requirement
                                    ),
                                    this.props.data.Currency,
                                    " ",
                                    this.props.data.minimumIncome_spreater,
                                    " ",
                                    this.props.data.gross_per_month
                                ),
                                React.createElement(
                                    "li",
                                    null,
                                    React.createElement(
                                        "strong",
                                        null,
                                        this.props.data.Employment_eligibility
                                    ),
                                    " ",
                                    this.props.data.field_employment_eligibility_tex
                                ),
                                React.createElement(
                                    "li",
                                    null,
                                    React.createElement(
                                        "strong",
                                        null,
                                        this.props.data.Age_requirement_min_max
                                    ),
                                    " ",
                                    this.props.data.minimumAge,
                                    " - ",
                                    this.props.data.maximumAge,
                                    " ",
                                    this.props.data.years_old
                                )
                            )
                        )
                    ),
                    React.createElement(
                        "div",
                        { className: "info-actions", "data-imu-track": "a" },
                        React.createElement(
                            "a",
                            { "data-href": this.props.data.field_apply_url, "data-url-product-page": this.props.data.product_path, href: this.props.data.field_apply_url, "data-img": this.props.data.bankLogo, "data-minage": this.props.data.minimumAge,
                                "data-maxage": this.props.data.maximumAge, "data-id": this.props.data.nid, "data-nid": this.props.data.nid, "data-interestrate": this.props.data.interestRate,
                                "data-installment": this.props.data.installment, "data-eligibility": this.props.data.field_employment_eligibility_tex,
                                "data-approvalduration": this.props.data.field_approval_duration, "data-minimumtenure": this.props.data.minimumTenure,
                                "data-maximumtenure": this.props.data.maximumTenure, "data-minimumincome": this.props.data.minimumIncome,
                                "data-minimumamount": this.props.data.minimumAmount, "data-maximumamount": this.props.data.maximumAmount, "data-highlight": this.props.data.field_product_highlight, className: "btn btn-success btn-lg btn-apply", "data-subject": "User",
                                "data-action": "Click", "data-applyFormType": this.props.data.formtype, "data-jappmachinename": this.props.data.jappMachineName,
                                "data-title": this.props.data.title, "data-value": this.props.data.title, "data-description": "Apply Button", "data-region": this.props.data.track_region, "data-vertical": "Personal Loan",
                                "data-bank": this.props.data.bank, "data-minirate": this.props.data.minIRate,
                                "data-maxirate": this.props.data.maxIRate, "data-islamic_conventional": this.props.data.islamic_conventional,
                                "data-cross-sell": this.props.data.field_pl_cross_sell_product, "data-product-path": this.props.data.product_path, className: this.props.data.quickApplyButton == "1" ? "btn btn-success btn-apply btn-qab" : "btn btn-success btn-apply" },
                            this.props.data.applyButtonText || this.props.data.Apply_Now
                        ),
                        React.createElement(
                            "a",
                            { href: "#", className: "btn-close" },
                            this.props.data.Close_panel
                        )
                    )
                )
            )
        );
    }
});


var divStyle = {
    height: "90px",
    width: "728px"
};

var PlAds = React.createClass({
    displayName: "PlAds",
    render: function () {
        return React.createElement(
            "div",
            { className: "table-columns a-center" },
            React.createElement("div", { className: "hidden-xs", id: "div-gpt-ad-1447813095536-0" }),
            React.createElement("div", { className: "visible-xs", id: "div-gpt-ad-1447813095536-4" })
        );
    },
    componentDidMount: function () {
        googletag.cmd.push(function () {
            googletag.display("div-gpt-ad-1447813095536-0");
        });
        googletag.cmd.push(function () {
            googletag.display("div-gpt-ad-1447813095536-4");
        });
    }
});


var HelloComponent = React.createClass({
    displayName: "HelloComponent",
    render: function () {
        var indents = [];
        for (var i = 0; i < numProps(this.props.params); i++) {
            if (i != 2) {
                indents.push(React.createElement(Plcompo, { key: i, data: this.props.params[i] }));
            }
            if (i == 2) {
                indents.push(React.createElement(Plcompo, { key: i, data: this.props.params[i] }));
                indents.push(React.createElement(PlAds, { key: "100" }));
            }
        }
        return React.createElement(
            "div",
            { className: "table__body" },
            indents
        );
        //return <span>Hello {this.props.employees.length}</span>;
    }
});


var Personalloan = React.createClass({
    displayName: "Personalloan",
    render: function () {
        return React.createElement(HelloComponent, { params: this.props.params });
    }
});

//var CommentBox = React.createClass({
//    render: function () {
//        return (
//            <pl watch-depth="value" params="tmp"></pl>
//        )
//    }
//});
//
//React.render(<CommentBox />, document.getElementById('Ehsan'));
