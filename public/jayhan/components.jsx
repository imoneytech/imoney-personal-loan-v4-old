function numProps(obj) {
    var c = 0;
    for (var key in obj) {
        if (obj.hasOwnProperty(key)) ++c;
    }
    return c;
}

var Plcompo = React.createClass({
    render: function() {

        return (
            //edit span here
            <div data-href={ this.props.data.field_apply_url}   data-cross-sell={ this.props.data.field_pl_cross_sell_product} data-bank={ this.props.data.bank} data-title={ this.props.data.title} data-url-product-page={this.props.data.product_path} data-img={ this.props.data.bankLogo}  data-minage={ this.props.data.minimumAge}
                 data-maxage={ this.props.data.maximumAge} data-product-path={this.props.data.product_path}  data-nid={this.props.data.nid} data-id={this.props.data.nid} data-interestrate={ this.props.data.interestRate}
                 data-installment={ this.props.data.installment} data-eligibility={ this.props.data.field_employment_eligibility_tex}
                 data-approvalduration={ this.props.data.field_approval_duration} data-minimumtenure={ this.props.data.minimumTenure}
                 data-maximumtenure={ this.props.data.maximumTenure} data-minimumincome={ this.props.data.minimumIncome_spreater}
                 data-highlight={ this.props.data.field_product_highlight} data-applyformtype= {this.props.data.formtype  }
                 data-minimumamount={ this.props.data.minimumAmount } data-maximumamount={ this.props.data.maximumAmount }
                 data-minimumAmount_spreater=   { this.props.data.minimumAmount_spreater } data-jappmachinename={this.props.data.jappMachineName} data-maximumAmount_spreater={ this.props.data.maximumAmount_spreater }
                 data-totalamountpayable={this.props.data.TotalAmountPayable} data-totalinterestpayable={ this.props.data.TotalInterestPayable}
                 className={this.props.data.className}>
                <div className={ this.props.data.col}><i></i>{ this.props.data.field_promotional_text}</div>
                <div className="table-columns">
                    <div className="col-compare col-sm-5p hidden-xs">
                        <div className="col-compare-btn"></div>
                    </div>
                    <div className="col-product"><img src={this.props.data.bankLogo} className="col-product-image"></img>
                        <div className="col-product-info">
                            <h3><div data-imu-track="a"><a href={["2965","2962","2961"].indexOf(this.props.data.nid) >= 0 ? "http://offers.imoney.my/campaign/personal-loan/affinbank-sme-financing" :"/"+this.props.data.product_path}data-subject="User"
                                   data-action="Click" data-description="Product Title" data-region="Listing Content Main" data-vertical="Personal Loan">{this.props.data.title}</a></div></h3>
                            <p>{this.props.data.field_product_highlight}</p><a href="" className="btn-more-info"><i></i><span>{this.props.data.More}</span> {this.props.data.info}</a>
                        </div>
                    </div>
                    <div className="col-rate col-sm-18p col-xxs-50p"><span className="col-rate--title col-title--small">{this.props.data.Interest_rate}</span>
                        <div className="col-rate--item font-xlg">{this.props.data.interestRate}%</div>
                    </div>
                    <div className="col-rate col-sm-18p col-xxs-50p"><span className="col-rate--title col-title--small">{this.props.data.Monthly_repayment}</span>
                        <div className="col-rate--item font-xlg">{this.props.data.Currency} {this.props.data.installment}</div><span className="product-repayment-period extra-info">{this.props.data.months} {this.props.data.translate_months}</span>
                    </div>
                    <div className="col-apply col-sm-18p" data-imu-track="a">
                        <a data-href={ this.props.data.field_apply_url} data-url-product-page={this.props.data.product_path} href={this.props.data.field_apply_url} rel="nofollow" data-img={ this.props.data.bankLogo}  data-minage={ this.props.data.minimumAge}
                           data-maxage={ this.props.data.maximumAge} data-id={this.props.data.nid} data-nid={this.props.data.nid} data-interestrate={ this.props.data.interestRate}
                           data-installment={ this.props.data.installment} data-eligibility={ this.props.data.field_employment_eligibility_tex}
                           data-approvalduration={ this.props.data.field_approval_duration} data-minimumtenure={ this.props.data.minimumTenure} data-maximumtenure={ this.props.data.maximumTenure} data-minimumincome={ this.props.data.minimumIncome }
                           data-minimumamount={ this.props.data.minimumAmount } data-maximumamount={ this.props.data.maximumAmount }
                           data-highlight={ this.props.data.field_product_highlight}data-jappmachinename={this.props.data.jappMachineName}
                           data-applyformtype= {this.props.data.formtype  } className="btn btn-success btn-apply" data-subject="User" data-action="Click"
                           data-title={this.props.data.title} data-value={this.props.data.title} data-description='Apply Button' data-region={this.props.data.track_region} data-vertical="Personal Loan"
                           data-bank = {this.props.data.bank} data-product-path={this.props.data.product_path}  data-minirate =  {this.props.data.minIRate} data-islamic_conventional={this.props.data.islamic_conventional}
                           data-maxirate =  {this.props.data.maxIRate} data-cross-sell={ this.props.data.field_pl_cross_sell_product} className={ this.props.data.quickApplyButton == '1' ? 'btn btn-success btn-apply btn-qab' : 'btn btn-success btn-apply'}>
                           {this.props.data.applyButtonText || this.props.data.Apply_Now}</a>
                    </div>
                </div>
                <div className="col-more-info">
                    <div className="info-summary">
                        <ul>
                            <li className="col-xxs-100 col-xs-50p"><strong>{ this.props.data.Summary }</strong>
                                {this.props.data.Currency} {this.props.data.filterAmount} {this.props.data.for} {this.props.data.filterTenure} {this.props.data.with_interest_rate_of} {this.props.data.interestRate}% {this.props.data.per_annum}
                            </li>
                            <li className="col-xxs-33p col-xs-14p"><strong>{ this.props.data.Monthly_repayment }</strong>{this.props.data.Currency} {this.props.data.installment}
                            </li>
                            <li className="col-xxs-33p col-xs-18p"><strong>{ this.props.data.Total_interest_payable }</strong>{this.props.data.Currency} {this.props.data.TotalInterestPayable}
                            </li>
                            <li className="col-xxs-33p col-xs-18p"><strong>{ this.props.data.Total_amount_payable }</strong>{this.props.data.Currency} {this.props.data.TotalAmountPayable}
                            </li>
                        </ul>
                    </div>
                    <div className="info-features">
                        <div className="features-key col-xs-50p">
                            <h5><i className="sprites sprites-star-blue"></i>{ this.props.data.Product_features}</h5>
                            <ul>
                                <li><strong>{ this.props.data.Financing_amount_min_to_max}</strong>{this.props.data.Currency} {this.props.data.minimumAmount_spreater} - { this.props.data.Currency } {this.props.data.maximumAmount_spreater}
                                </li>
                                <li><strong>{ this.props.data.Loan_tenure_min_to_max}</strong>{this.props.data.minimumTenure} - {this.props.data.maximumTenure} {this.props.data.years}
                                </li>
                                <li><strong>{ this.props.data.Interest_rate}</strong>{this.props.data.Fixed_rate_of} {this.props.data.minIRate}% - {this.props.data.maxIRate}% {this.props.data.per_annum}
                                </li>
                                <li><strong>{ this.props.data.Approval_duration}</strong>{this.props.data.field_approval_duration}
                                </li>
                            </ul>
                        </div>
                        <div className="features-eligibility col-xs-50p">
                            <h5><i className="sprites sprites-tick-blue"></i>{this.props.data.Eligibility}</h5>
                            <ul>
                                <li><strong>{this.props.data.Min_salary_requirement}</strong>{this.props.data.Currency} {this.props.data.minimumIncome_spreater} {this.props.data.gross_per_month}
                                </li>
                                <li><strong>{this.props.data.Employment_eligibility}</strong> {this.props.data.field_employment_eligibility_tex}
                                </li>
                                <li><strong>{ this.props.data.Age_requirement_min_max}</strong> {this.props.data.minimumAge} - {this.props.data.maximumAge} {this.props.data.years_old}
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div className="info-actions" data-imu-track="a" >
                        <a data-href={this.props.data.field_apply_url} data-url-product-page={this.props.data.product_path} href={this.props.data.field_apply_url} data-img={ this.props.data.bankLogo}  data-minage={ this.props.data.minimumAge}
                           data-maxage={ this.props.data.maximumAge} data-id={this.props.data.nid} data-nid={this.props.data.nid} data-interestrate={ this.props.data.interestRate}
                           data-installment={ this.props.data.installment} data-eligibility={ this.props.data.field_employment_eligibility_tex}
                           data-approvalduration={ this.props.data.field_approval_duration} data-minimumtenure={ this.props.data.minimumTenure}
                           data-maximumtenure={ this.props.data.maximumTenure} data-minimumincome={ this.props.data.minimumIncome}
                           data-minimumamount={ this.props.data.minimumAmount } data-maximumamount={ this.props.data.maximumAmount }  data-highlight={ this.props.data.field_product_highlight} className="btn btn-success btn-lg btn-apply" data-subject="User"
                           data-action="Click" data-applyFormType= {this.props.data.formtype} data-jappmachinename={this.props.data.jappMachineName}
                           data-title={this.props.data.title} data-value={this.props.data.title} data-description='Apply Button' data-region={this.props.data.track_region} data-vertical="Personal Loan"
                           data-bank = {this.props.data.bank} data-minirate =  {this.props.data.minIRate}
                           data-maxirate =  {this.props.data.maxIRate} data-islamic_conventional={this.props.data.islamic_conventional}
                           data-cross-sell={ this.props.data.field_pl_cross_sell_product} data-product-path={this.props.data.product_path}  className={ this.props.data.quickApplyButton == '1' ? 'btn btn-success btn-apply btn-qab' : 'btn btn-success btn-apply'}>
                           {this.props.data.applyButtonText || this.props.data.Apply_Now}</a>
                           <a href="#" className="btn-close">{this.props.data.Close_panel}
                    </a>
                    </div>
                </div>
            </div>
        );
    }
})


var divStyle = {
    height:'90px',
    width:'728px'
};

var PlAds = React.createClass({
    render: function() {
        return (
            <div className="table-columns a-center">
                    <div className="hidden-xs" id='div-gpt-ad-1447813095536-0'>
                    </div>
                    <div className="visible-xs" id='div-gpt-ad-1447813095536-4'>
                    </div>
            </div>);
    },
    componentDidMount: function() {
        googletag.cmd.push(function() { googletag.display('div-gpt-ad-1447813095536-0'); });
        googletag.cmd.push(function() { googletag.display('div-gpt-ad-1447813095536-4'); });
    }
})


var HelloComponent = React.createClass({
    render: function() {
            var indents = [];
        for (var i = 0; i < numProps(this.props.params); i++ ) {
            if ( i!= 2) {
                indents.push(< Plcompo key={i} data={this.props.params[i]}/>);
            }
            if ( i== 2){
                indents.push(< Plcompo key={i} data={this.props.params[i]}/>);
                indents.push(<PlAds key='100' />);
            }
        }
        return (
            <div className='table__body'>
                {indents}
            </div>
        );
        //return <span>Hello {this.props.employees.length}</span>;
    }
})


var Personalloan = React.createClass({
    render: function() {
        return (
            <HelloComponent params={this.props.params} />
        );
    }
});

//var CommentBox = React.createClass({
//    render: function () {
//        return (
//            <pl watch-depth="value" params="tmp"></pl>
//        )
//    }
//});
//
//React.render(<CommentBox />, document.getElementById('Ehsan'));
