$(document).ready(function () {
    var data = localStorage.getItem("data");
    var pldatas= JSON.parse(data);
        product_id = [],
        product_name = [],
        product_brand = [],
        product_category = [],
        product_sub_category = [],
        product_pl_rate = [],
        product_bb_speed = [];

    pldatas.forEach(function(element) {
        product_id.push(element.nid);
        product_name.push(element.title);
        product_brand.push(element.bank);
        product_category.push('Personal Loan');
        product_sub_category.push(element.islamic_conventional);
        product_pl_rate.push(element.interestRate);
    });

    dataLayer.push({
        event: 'viewProductList',
        event_lang: $('html').attr('lang'),
        content_type : "product_group",
        track_id: userManager.data.trackId,
        list_type: 'listing',
        list_name: Lising_url.replace("ms/", ""),
        list_size: pldatas.length,
        product_id: product_id,
        product_name: product_name,
        product_brand: product_brand,
        product_category: product_category,
        product_sub_category: product_sub_category,
        product_pl_rate: product_pl_rate,
        user_device :WURFL.form_factor,
        referrer_source : userManager.utms.utmcsr,
        referrer_medium : userManager.utms.utmcmd
    });

    $('body').on('click', '.btn-apply', function(e){
        console.log('btn-apply');
        var data = {
            event : 'applyNow',
            product_id : $(this).attr('data-id'),
            product_name: $(this).attr('data-title'),
            product_brand : $(this).attr('data-bank'),
            product_category : "Personal Loan",
            product_sub_category:$(this).attr('data-islamic_conventional') ,
            product_pl_rate : $(this).attr('data-interestrate'),
            track_id : userManager.data.trackId,
            event_lang :lang,
            user_device :WURFL.form_factor,
            referrer_source : userManager.utms.utmcsr,
            referrer_medium : userManager.utms.utmcmd
        };
        dataLayer.push(data);

    });


    $( "#btn-pl-submit" ).click(function() {
        if($('#applyForm').parsley().isValid()) {
            var data = {
                event : 'submitDetails',
                submit_type : lang+'_pl_listing',
                product_id : $(this).attr('data-nid'),
                product_name: $(this).attr('data-title'),
                product_brand : $(this).attr('data-bank'),
                product_category : 'Personal Loan',
                product_sub_category:$(this).attr('data-islamic_conventional') ,
                product_pl_rate : $(this).attr('data-interestrate') ,
                track_id : userManager.data.trackId,
                event_lang : lang,
                user_device :WURFL.form_factor,
                referrer_source : userManager.utms.utmcsr,
                referrer_medium : userManager.utms.utmcmd
            };
            dataLayer.push(data);
        }

    });




});