const webpack = require('webpack');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin')

config = {
    context: __dirname,
    entry: ["babel-polyfill" ,"./vue-app/main.js"],
    output: {
        path: __dirname + "/public/jayhan/vue-dist",
        filename: 'build.js'
    },
    module: {
        rules: [{
                test: /\.js$/,
                loader: 'babel-loader?presets=es2015',
                exclude: /node_modules/
            },
            {
                test: /\.vue$/,
                loader: 'vue-loader',
            }
        ]
    },
    resolve: {
        alias: {
            'vue$': 'vue/dist/vue.esm.js',
        }
    },
    plugins: [
    ]
};

if (process.env.NODE_ENV === 'production') {
    config.plugins.push(
         new webpack.DefinePlugin({
            'process.env': {
                NODE_ENV: '"production"'
            }
        }),
        new UglifyJsPlugin({
            sourceMap: true
        })
    )
}

module.exports = config 