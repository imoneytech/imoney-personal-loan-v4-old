'use strict';

/**
 * Module dependencies.
 * @private
 */
var
    Backbone = require('backbone'),
    request = require('request');

var metadata = Backbone.Model.extend({
    idAttribute: "nid"
});


/**
 * Module exports.
 * @public
 */

exports = module.exports = function(url) {
    console.log('getting metadata listing pages .. !!');
    console.log('url = '+url);
    return new (Backbone.Collection.extend({
        model: metadata,
        "url": url,  //TODO: cache to production url
        fetch: function(callback) {
            var that = this;
            request(url, function (error, response, body) {
                //console.log(body);
                if (!error && response.statusCode == 200) {
                    var jsonData = JSON.parse(body);
                     that.set(jsonData);
                }
                if(typeof callback === 'function') {
                    callback(error, null);
                }
            })
            .on('error', function(err) {
                console.log(err)
            });
        }
    }));
};