'use strict';

/**
 * Module dependencies.
 * @private
 */
var
    Backbone = require('backbone'),
    request = require('request');

var Product = Backbone.Model.extend({
    idAttribute: "nid",
    url: function () {
        return this;
    }
});


/**
 * Module exports.
 * @public
 */



exports = module.exports = function (url) {
    return new (Backbone.Collection.extend({
        model: Product,
        initialize: function () {
            // This will be called when an item is added. pushed or unshifted
            this.on('add', function (model) {
            });
            // This will be called when an item is removed, popped or shifted
            this.on('remove', function (model) {
                console.log('something got removed');
            });
            // This will be called when an item is updated
            this.on('change', function (model) {
                console.log('something got changed');
            });
        },
        "url": url,  //TODO: cache to production url
        fetch: function (callback) {
            var that = this;
            request(url, function (error, response, body) {
                if (!error && response.statusCode == 200) {
                    that.set(JSON.parse(body)['rates']);
                    // callback(null, JSON.parse(body)['rates']);
                }
            })
            .on('error', function(err) {
                console.log(err)
            });
        }
    }));
};