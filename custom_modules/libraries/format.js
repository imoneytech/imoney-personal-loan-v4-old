var swig = require('swig'),
    languages = require('./languages'),
    extras = require('swig-extras'),
    currencies = {
        my: {
            split: ',',
            currency: 'RM'
        },
        sg: {
            split: ',',
            currency: 'SGD'
        },
        id: {
            split: '.',
            currency: 'Rp.'
        },
        ph: {
            split: ',',
            currency: '₱'
        }
    },
    regionNumber = function (number, region) {
        var digit = currencies[region].split == '.' ? ',' : '.';
        number = number.toString().replace('.', digit);
        return number.replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1" + currencies[region].split);
    },
    iMoney = function (money, region) {
        if (money) {
            return currencies[region].currency + money.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1" + currencies[region].split);
        }
        else {
            return languages.translate('Free', region);
        }
    };

extras.useFilter(swig, 'markdown');
swig.setFilter('regionNumber', regionNumber);
swig.setFilter('iMoney', iMoney);

module.exports = {
    regionNumber: regionNumber,
    iMoney: iMoney
};