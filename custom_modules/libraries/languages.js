var swig = require('swig'),
  _ = require('underscore'),
  fs = require('fs'),
  getText = require('node-gettext'),
  gettextParser = require('gettext-parser'),
  translateRegexes = {
    withoutContext: /\{\{ ?(\"(.+?)\"|\'(.+?)\') ?\| ?translate.*?}}/gi,
    withContext: /\{\{ ?(\"(.+?)\"|\'(.+?)\') ?\| ?translate\((\"(.+?)\"|\'(.+?)\'|null).*\).*?}}/gi,
    function: /translate\((\"(.+?)\"|\'(.+?)\')(, (\"(.+?)\"|\'(.+?)\'))?\)/gi
  },
  config = require('konfig')(),
  region = config.app.region,
  language = config.app.lang;

try {
  var translator = new getText();

  var translateData = fs.readFileSync('dev/translation/' + config.app.lang + '_' + config.app.region.toUpperCase() + '.po');
  translator.textdomain(region);
  translator.addTextdomain(region, translateData);
}
catch (e) {
  console.log(e);
  translator = false;
}

// Function for translating.
var translate = function (text, context, params) {
    // Auto set params.
    params = (typeof params !== 'undefined') ? params : {};
    if (translator) {
      if (typeof context !== 'undefined' && context != null && context.length) {
        var translated = translator.pgettext(context, text);
      }
      else {
        var contextTranslated = translator.pgettext(global.config.app.currentTemplate, text);
        var translated = text != contextTranslated ? contextTranslated : translator.gettext(text);
      }
    }
    else {
      var translated = text;
    }

    // Add params.
    _.each(params, function (value, key) {
      translated = translated.replace(key, value);
    });
    return translated;
  },

// Function for generating POT files.
  potGenerate = function () {
    var translateList = {
        noContext: {},
        context: {}
      },
      translateData = '',
      folders = [
        'views',
        'bower_components/imoney-assets/dev/templates',
        'bower_components/imoney-assets/dev/js',
        'routes',
        'custom_modules',
        'dev/js',
        'dev/jade',
        'public/jayhan/angular-service'
      ];

    // Read all files in template folders.
    _.each(folders, function (folder) {
      translateList = readFolder(folder, translateList);
    });

    // Write translate data to a file.
    _.each(translateList.noContext, function (msgid) {
      translateData += "msgid \"" + msgid + "\"\n" +
        "msgstr \"\"" +
        "\n" +
        "\n";
    });

    _.each(translateList.context, function (translate) {
      translateData += "msgctxt \"" + translate.msgctxt + "\"\n" +
        "msgid \"" + translate.msgid + "\"\n" +
        "msgstr \"\"" +
        "\n" +
        "\n";
    });
    fs.writeFileSync('dev/translation/'+ config.app.lang + '_' + config.app.region.toUpperCase()+'.pot', translateData);
    //fs.writeFileSync('dev/translation/en_MY.pot', translateData);
  },

// Function for generating JSON file base on PO   file.
  jsonGenerate = function () {
    var fileStandard = language + '_' + region.toUpperCase();
    try {
      var poFile = require('fs').readFileSync('dev/translation/' + fileStandard + '.po');
      var poData = gettextParser.po.parse(poFile);
      return poData.translations;
    }
    catch (e) {
      return {};
    }
  };

var readFolder = function (folder, translateList) {
  var items = fs.readdirSync(folder);
  _.each(items, function (item) {
    var stat = fs.statSync(folder + '/' + item);
    if (stat.isDirectory()) {
      translateList = readFolder(folder + '/' + item, translateList);
    }
    else if (stat.isFile() && item != 'languages.js') {
      var data = fs.readFileSync(folder + '/' + item, 'utf-8');
      // Regex in template without context.
      while ((result = translateRegexes.withoutContext.exec(data))) {
        var translateString = result[2] || result[3];
        translateString = translateString.replace(/\"/g, '\\\"');
        translateList.context[item + ':' + translateString] = {
          msgctxt: item,
          msgid: translateString
        };
      }

      // Regex in template with context.
      while ((result = translateRegexes.withContext.exec(data))) {
        var translateString = result[2] || result[3];
        translateString = translateString.replace(/\"/g, '\\\"');
        var translateContext = result[5] || result[6];
        if (typeof translateContext !== 'undefined' && translateContext != 'URL' && item.indexOf('.js') == -1) {
          translateList.context[translateContext + ':' + translateString] = {
            msgctxt: translateContext,
            msgid: translateString
          };
        }
        else {
          translateList.context[item + ':' + translateString] = {
            msgctxt: item,
            msgid: translateString
          };
        }
      }

      while ((result = translateRegexes.function.exec(data))) {
        var translateString = result[2] || result[3];
        translateString = translateString.replace(/\"/g, '\\\"');
        var translateContext = result[6] || result[7];
        if (typeof translateContext !== 'undefined' && translateContext != 'URL') {
          translateList.context[translateContext + ':' + translateString] = {
            msgctxt: translateContext,
            msgid: translateString
          };
        }
        else {
          translateList.noContext[translateString] = translateString;
        }
      }
    }
  });

  return translateList;
};

swig.setFilter('translate', translate);

module.exports = {
  translate: translate,
  potGenerate: potGenerate,
  jsonGenerate: jsonGenerate
};