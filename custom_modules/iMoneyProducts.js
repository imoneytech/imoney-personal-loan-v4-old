'use strict';

var http = require('q-io/http');
var Q = require('q');
var _ = require('underscore');

var products = [], globalProductType = '';

var fetchProducts = function (productType) {
    var deferred = Q.defer();
    // If product data is cached, return cached version.
    if (products[productType]) {
        deferred.resolve(products[productType]);
    }
    else {
        var types = {
            creditCards: {
                url: 'https://www.imoney.my/credit-card.json',
                key: 'rates'
            },
            homeLoans: {
                url: 'https://www.imoney.my/home-loan.json',
                key: 'rates'
            },
            broadBand: {
                url: 'https://www.imoney.my/profile/getBroadbandJson',
                key: 'broadband'
            },
            credit_card: {
                url: 'http://direct.imoney.my/profile/getCreditCard/my',
                key: 'cards'
            }
        };
        var url = types[productType].url,
            key = types[productType].key;

        // Q-IO HTTP returns a promise object after request is done.
        http.request(url).then(function (response) {
            response.body.read().then(function (stream) {
                // Parse JSON string.
                var json;
                json = JSON.parse(stream.toString('utf8'));

                // Store product data in cache.
                products[productType] = json[key];

                // Notify the deferred object that we're done and good to go.
                deferred.resolve(json[key]);
            }).fail(function (error) {
                // Catch any errors.
                console.log(error);
            });
        });
    }
    return deferred.promise;
};

var fetchProductsByRegions = function (productType, region) {
    var deferred = Q.defer();
    // If product data is cached, return cached version.
    if (products[productType]) {
        deferred.resolve(products[productType]);
    }
    else {
        var types = {
            credit_card: {
                url: 'http://direct.imoney.my/profile/getCreditCard/' + region,
                key: 'cards'
            }
        };
        var url = types[productType].url,
            key = types[productType].key;

        // Q-IO HTTP returns a promise object after request is done.
        http.request(url).then(function (response) {
            response.body.read().then(function (stream) {
                // Parse JSON string.
                var json;
                json = JSON.parse(stream.toString('utf8'));

                // Store product data in cache.
                products[productType] = json[key];

                // Notify the deferred object that we're done and good to go.
                deferred.resolve(json[key]);
            }).fail(function (error) {
                // Catch any errors.
                console.log(error);
            });
        });
    }
    return deferred.promise;
};

module.exports = {
    fetch: function (productType) {
        globalProductType = productType;
        return this;
    },
    products: function (filter) {
        // If filter exists, filter products before returning.
        return fetchProducts(globalProductType).then(function (products) {
            if (filter) {
                // Underscore is awesome!
                return _.where(products, filter);
            }
            else {
                return products;
            }
        });
    },
    productsByRegions: function (region, filter) {
        // If filter exists, filter products before returning.
        return fetchProductsByRegions(globalProductType, region).then(function (products) {
            if (filter) {
                // Underscore is awesome!
                return _.where(products, filter);
            }
            else {
                return products;
            }
        });
    },
    banks: function () {
        // Get all unique banks from list of products.
        return fetchProducts(globalProductType).then(function (products) {
            var banks = [];

            products.forEach(function (product) {
                if (banks.indexOf(product.bank) < 0) {
                    banks.push(product.bank);
                }
            });
            banks = banks.sort();
            return banks;
        });
    },
    telcos: function () {

        // Get all unique telcos from list of products.
        return fetchProducts(globalProductType).then(function (products) {
            var telcos = [];

            products.forEach(function (product) {
                if (telcos.indexOf(product.field_telco) < 0) {
                    telcos.push(product.field_telco);
                }
            });
            telcos = telcos.sort();
            return telcos;
        });
    }
};
