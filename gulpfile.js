var gulp = require('gulp'),
    rename = require('gulp-rename'),
    gap = require('gulp-append-prepend'),
   uglify = require('gulp-uglify');

gulp.task('minify', function () {
   gulp.src('dev/js/personal-loan-refactor.js')
        .pipe(gap.prependFile('bower_components/imoney-assets/bower_components/nouislider/distribute/nouislider.min.js'))
        .pipe(gap.prependFile('bower_components/imoney-assets/bower_components/nouislider/documentation/assets/wNumb.js'))
        .pipe(gap.prependFile('bower_components/imoney-assets/bower_components/bootstrap-sass/assets/javascripts/bootstrap/transition.js'))
        .pipe(gap.prependFile('bower_components/imoney-assets/bower_components/bootstrap-sass/assets/javascripts/bootstrap/modal.js'))
        .pipe(gap.prependFile('dev/js/stickykit.min.js'))
        .pipe(gap.prependFile('dev/js/jquery.easing.min.js'))
        .pipe(rename('personal-loan-refactor.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('public/jayhan'))
});

gulp.task('minify-product', function () {
    gulp.src('dev/js/personal-loan__product.js')
         .pipe(gap.prependFile('bower_components/imoney-assets/bower_components/nouislider/distribute/nouislider.min.js'))
         .pipe(gap.prependFile('bower_components/imoney-assets/bower_components/nouislider/documentation/assets/wNumb.js'))
         .pipe(gap.prependFile('bower_components/imoney-assets/bower_components/bootstrap-sass/assets/javascripts/bootstrap/transition.js'))
         .pipe(gap.prependFile('bower_components/imoney-assets/bower_components/bootstrap-sass/assets/javascripts/bootstrap/modal.js'))
         .pipe(gap.prependFile('dev/js/stickykit.min.js'))
         .pipe(gap.prependFile('dev/js/jquery.easing.min.js'))
         .pipe(rename('personal-loan__product.min.js'))
         .pipe(uglify())
         .pipe(gulp.dest('public/jayhan'))
 });